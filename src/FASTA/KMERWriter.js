const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input must be in SAM format
 * Test only done for Bowtie2 output
**/

class KMERWriter extends Transform {
    constructor(options) {
        super(options);
        this._chunk = "";
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        let ret = "";
        if(!felem.ID.startsWith(">"))
            ret += ">";
        ret += felem.ID + "\n";
        ret += felem.kmer + "\n";
        this.push(ret);
        done();
    }
}
module.exports=KMERWriter;