const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input must be in SAM format
 * Test only done for Bowtie2 output
**/

class FASTAkmers extends Transform {
    constructor(options,size=20,overlap=5) {
        super(options);
        this._chunk = "";
        this.k = size;
        this.overlap=overlap;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        var kmers = this.getKmers(felem.def,felem.seq);
        for(let i=0; i<kmers.length; i++)
            this.push(JSON.stringify(kmers[i]));
        done();
    }
    getKmers(id,seq) {
        var kmers = [];
        for(var i=0; i<seq.length-this.k; i+=this.overlap) {
            kmers.push({'ID' : id+"_k_"+i, 
                        'kmer': seq.substr(i,this.k)
                      });
        }
        return kmers;
    }    
}
module.exports=FASTAkmers;