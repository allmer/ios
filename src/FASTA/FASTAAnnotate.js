const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const split2 = require('split2');
const FASTAElement = require("./FASTAElement");
const FASTAAggregate = require("./FASTAAggregate");

/** Assumptions
 * input comes from blast
 * blast -outfmt "6 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore" //is the only supported input for this.
 * 
**/

class FASTAAnnotate extends Transform {
    constructor(options, delimiter="|") {
        super(options);
        this._chunk = "";
        this.queries = null;
        this.mapping = {};
        this.reference = null;
        this.curDef = null;
        this.bestRes = null;
        this.delimiter = delimiter;
        this.nomatch = ["na","na","na","na","na","na","na","na","na","na","na","na","na","na"];
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let dta = strChunk.split("\t");
        if(!dta[0].startsWith(">")) dta[0] = ">"+dta[0];
        let effAl = dta[4]-(dta[4]-dta[3]); //effective alignment length (alignlen - (alignlen-ident));
        dta.push(effAl);
        if(this.curDef === null) {
            this.curDef = dta[0];
            this.bestRes = dta;            
        }
        if(dta[0] !== this.curDef) {
            this.annotate(this).then((res)=>{
                this.curDef = dta[0];
                this.bestRes = dta;     
                done();           
            }).catch(err=>{
                console.log("Error",err);
                console.log("def",this.curDef);
                console.log("best",this.bestRes);
                console.log("in",strChunk);
            });
        } else {
            if(effAl > this.bestRes[13]) {
                this.bestRes = dta;       
                //console.log(this.bestRes);
            }
            done();
        }
    }  
    
    _flush = function(done) {
        this.annotate(this).then((val)=>{
            for(let key in this.queries) {
                let fe = this.queries[key];
                fe.def +=  this.delimiter + "na";
                this.push(JSON.stringify(fe));
                if(this.reference !== null) {
                    if(key in this.reference) {
                        this.mapping[key] = this.reference[key].concat(this.nomatch);
                    } else {
                        this.mapping[key] = [];
                        this.mapping[key].push(key);
                        this.mapping[key] = this.mapping[key].concat(this.nomatch);
                    }
                } else {
                    this.mapping[key] = [];
                    this.mapping[key].push(key);
                    this.mapping[key] = this.mapping[this.curDef].concat(this.nomatch);
                }
            }  
            done();          
        });
    }    
    
    annotate(stream) {
        let self = this;
        return new Promise((resolve,reject) => {
            let fe = self.queries[self.curDef];
            fe.def +=  self.delimiter + self.bestRes[2];
            //console.log(self.queries);
            delete self.queries[self.curDef];
            stream.push(JSON.stringify(fe));
            if(self.reference !== null) {
                if(self.curDef in self.reference) {
                    self.mapping[self.curDef] = self.reference[self.curDef].concat(self.bestRes);
                } else {
                    self.mapping[self.curDef] = [];
                    self.mapping[self.curDef].push(fe.def);
                    self.mapping[self.curDef] = self.mapping[self.curDef].concat(self.bestRes);
                }
            } else {
                self.mapping[self.curDef] = [];
                self.mapping[self.curDef].push(fe.def);
                self.mapping[self.curDef] = self.mapping[self.curDef].concat(self.bestRes);
            }
            resolve(true);
        });
    }
    
    setMapping(mappingFile,bar=null,divider="|") {
        return new Promise((resolve,reject) => {
            if(mappingFile === null || !fs.existsSync(mappingFile))
                reject("no reference provided or file not accessible: "+mappingFile);
            const data = fs.readFileSync(mappingFile,'UTF-8');
            const lines = data.split(/\r?\n/);
            if(bar !== null) bar.total = lines.length;
            this.reference = {};
            lines.forEach((line) => {
                let dta = line.split(divider);
                let curDef = dta[0].trim();
                if(!curDef.startsWith(">"))
                    curDef = ">"+curDef;
                this.reference[curDef]=[dta[1]];
                if(bar !== null) bar.tick(1);
            });
            resolve(true);
        });
    }
    
    getMapping() {
        return this.mapping;
    }
    
    setQueries(fastaFile,bar=null) {
        return new Promise((resolve,reject) => {
            let instream = fs.createReadStream(fastaFile);
            let fe = new FASTAElement(null);
            let fagg = new FASTAAggregate(null);
            pipeline(
                    instream,
                    split2(),
                    fe,
                    fagg,
                    (err) => {
                        if(err) {
                            reject(err);   
                        } else {
                           this.queries = fagg.FASTAElements; 
                           resolve(true);
                        }
                    }
            );
        });
    }
}
module.exports=FASTAAnnotate;