const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTASeqType extends Transform {
    constructor(options,checkMax=100,permissableCharacters =['*','-','.'],forward="both") {
        super(options);
        this._chunk = "";
        this.nucleotides = {"A":1,"C":1,"G":1,"T":1,"U":1,"R":1,"Y":1,"S":1,"W":1,"K":1,"M":1,"B":1,"D":1,"H":1,"V":1,"N":1}; //IUPAC
        this.aminoacids = {"A":1,"C":1,"D":1,"E":1,"F":1,"G":1,"H":1,"I":1,"K":1,"L":1,"M":1,"N":1,"P":1,"Q":1,"R":1,"S":1,"T":1,"V":1,"W":1,"Y":1};
        this.permissables = {};
        for(let i=0; i<permissableCharacters.length; i++)
            this.permissables[permissableCharacters[i]] = 1;
        this.checkMax = checkMax;
        this.type = null;
        this.forward = forward;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        let seq = felem.seq.replace(/\s+/g, '');
        this.type = "nucleotide";
        for(let i=0; i<this.checkMax && i<seq.length; i++) {
            let c = seq[i];
            if(!(c in this.nucleotides) && (c in this.aminoacids)) {
                this.type = "aminoacid";
                felem["type"]=this.type;
                if((this.forward === "both") || (this.forward === this.type))
                    this.push(JSON.stringify(felem));             
                break;
            } else {
                if(!(c in this.nucleotides) && !(c in this.aminoacids) && !(c in this.permissables)) {
                    console.log("about to throw");
                    throw new Error("UNEXPECTED CHARACTER");//"Unexpected char in sequence (" + felem.def + "): " + c + "."});
                }
            }

        }
        done();
    }  
}
module.exports=FASTASeqType;