const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTADefLineFilter extends Transform {
    constructor(options,firstNum=0,prefix=null,suffix=null) {
        super(options);
        this._chunk = "";
        this.numbering = firstNum;
        this.prefix = ">"; 
        this.suffix = "\n";
        if(suffix !== null)
            this.suffix = suffix+this.suffix;
        if(prefix !== null)
            this.prefix += prefix;
        this.reference = null;
        this.mapping = {};
        this.errors = [];
        this.err = 0;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        //console.log(felem.def);
        if(this.reference !== null) {
            let newDef = this.reference[felem.def.trim()];
            if(!newDef) {
                this.errors[this.err++] = felem.def + " not found in reference; so kept original definition line.";
                this.push(strChunk);
                done();
            } else {
                this.mapping[newDef]=felem.def;
                felem.def = newDef;
                this.push(JSON.stringify(felem));
                done();
            }
        } else {
            //console.log(this.numbering);
            let newDef = this.prefix;
            newDef += this.numbering++;
            newDef += this.suffix;
            this.mapping[newDef.trim()]=felem.def;
            //console.log(felem.def);
            felem.def = newDef.trim();
            //console.log(felem.def);
            this.push(JSON.stringify(felem));
            done();
        }
    }  
    
    getErrors() {
        return this.errors;
    }
    
    /**
     * In order to rename the definition lines according to a reference,
     * provide a list with the current definition line and the desired definition line.
     * Example:
     * >identifier for some part|>1
     * >some other id|>ADFJ132
     * @param {File} mappingFile the filename which contains the mapping
     * @returns {undefined}
     */
    setMapping(mappingFile,bar=null,divider="|") {
        return new Promise((resolve,reject) => {
            if(mappingFile === null || !fs.existsSync(mappingFile))
                reject("no reference provided or file not accessible: "+mappingFile);
            this.reference = {};
            const data = fs.readFileSync(mappingFile,'UTF-8');
            const lines = data.split(/\r?\n/);
            if(bar !== null) bar.total = lines.length;
            lines.forEach((line) => {
                let dta = line.split(divider);
                let curDef = dta[0].trim();
                if(!curDef.startsWith(">"))
                    curDef = ">"+curDef;
                let newDef = dta[1].trim();
                if(!newDef.startsWith(">"))
                    newDef = ">" + newDef;
                this.reference[curDef] = newDef; 
                if(bar !== null) bar.tick(1);
            });
            resolve(true);
        });
    }
}
module.exports=FASTADefLineFilter;