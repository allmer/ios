const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTASplitter extends Transform {
    constructor(options,baseName="split%i.fasta",numQueries=1000,line=80) {
        super(options);
        this._chunk = "";
        this.index=1;
        this.baseName = baseName;
        this.numQueries = numQueries;
        this.curFile = this.baseName.replace("%i",this.index.toString());
        this.curStream = fs.openSync(this.curFile,'w');
        this.index++;
        this.qi = 1;
        this.line = line;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003') {
            fs.closeSync(this.curStream);
            process.exit();
        }
        let felem = JSON.parse(strChunk);
        if(this.qi === this.numQueries) {
            fs.writeSync(this.curStream,felem.def+"\n"+this.prepSeq(felem.seq,this.line));
            fs.closeSync(this.curStream);
            this.qi = 1;
            this.curFile = this.baseName.replace("%i",this.index.toString());
            this.index++;
            this.curStream = fs.openSync(this.curFile,"w");
            done();
        } else {
            fs.writeSync(this.curStream,felem.def+"\n"+this.prepSeq(felem.seq,this.line));
            this.qi++;
            done();
        }
    }  
    prepSeq(seq,linelength) {
        let ret = "";
        let seqlen = seq.length-linelength;
        if(seqlen < 0)
            return seq + "\n";
        let i = 0;
        for(i=0; i<seqlen; i+=linelength) {
            ret += seq.substring(i,(i+linelength)) + "\n";
        }
        ret += seq.substring(i);
        if(!ret.endsWith("\n"))
            ret += "\n";
        return ret;
    }
}
module.exports=FASTASplitter;