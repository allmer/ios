const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTAWriter extends Transform {
    constructor(options,line=null) {
        super(options);
        this._chunk = "";
        this.line = Number(line);
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        if(!felem.def.startsWith(">"))
            this.push(">");
        this.push(felem.def); 
        this.push("\n");       
        if(this.line === null) {
            this.push(felem.seq);
            this.push("\n");
        } else {
            let i=0; 
            let seqlen = felem.seq.length-this.line;
            for(i=0; i<seqlen; i+=this.line) {
                let tp = felem.seq.substring(i,(i+this.line));
                this.push(tp);
                this.push("\n");
            }
            this.push(felem.seq.substring(i));
            this.push("\n");
        }
        done();
    }
}
module.exports=FASTAWriter;