const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTALenFilter extends Transform {
    constructor(options,minLen=100,maxLen=-1) {
        super(options);
        this._chunk = "";
        this.minLen = minLen;
        this.maxLen = maxLen;
        this.filtered = 0;
        this.passed = 0;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        if(felem.seq.length > this.minLen && ((felem.seq.length < this.maxLen) || (this.maxLen === -1))) {
            this.push(JSON.stringify(felem));
            this.passed++;
        } else
            this.filtered++;
        done();
    }  
}
module.exports=FASTALenFilter;