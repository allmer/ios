const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTAAggregate extends Transform {
    constructor(options) {
        super(options);
        this._chunk = "";
        this.FASTAElements = {};
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        this.FASTAElements[felem.def.split(" ")[0]]=felem;
        done();
    }  
}
module.exports=FASTAAggregate;