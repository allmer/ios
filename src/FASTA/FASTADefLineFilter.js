const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTADefLineFilter extends Transform {
    constructor(options,max=null) {
        super(options);
        this._chunk = "";
        this.max = max;
    }
    
    _transform(chunk,encoding,done) {
        if(max !== null && max <= 0)
            throw "NUMBER_INVALID";
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        if(this.max === null) {
            this.push(felem.def);
            this.push("\n");
        } else {
            if(this.max > 0) {
                this.push(felem.def);
                this.push("\n");
                this.max--;
            } else {
                throw "NORMAL_TERMINATION_SET_LIMIT_REACHED";
            }
        }
        done();
    }  
}
module.exports=FASTADefLineFilter;