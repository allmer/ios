const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
 * species need to be provided for filtering. They need to be provided in an associative array:
            {
              '1815267': 'Amphilimna polyacantha',
              '2566087': 'Amphilimna granulosa',
              'Amphilimna polyacantha': '1815267',
              'Amphilimna granulosa': '2566087'
            }
 * The fasta file to be filtered must contain tax id or species name for successful filtering:
 * >UniRef90_A0A5A9P0L4 Peptidylprolyl isomerase n=1 Tax=Triplophysa tibetana TaxID=1572043 RepID=A0A5A9P0L4_9TELE
**/

class FASTATaxFilter extends Transform {
    constructor(options,species,taxidPrefix="TaxID=",taxidPostfix=" ",speciesNamePrefix="Tax=",speciesNamePostfix=" ") {
        super(options);
        this._chunk = "";
        this.species = species;
        this.taxidPrefix = taxidPrefix;
        this.taxidPostfix = taxidPostfix;
        this.speciesNamePrefix = speciesNamePrefix;
        this.speciesNamePostfix = speciesNamePostfix;
        console.log("Filter constructed.");
        this.fastaSections = {"accepted":0,"rejected":0};
        
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        let deflk = felem.def;
        let ts = deflk.indexOf(this.taxidPrefix);
        let acc = false;
        if(ts !== -1) {
            ts += this.taxidPrefix.length;
            let te = deflk.indexOf(this.taxidPostfix,ts);
            let tid = "";
            if(te === -1) 
                tid = deflk.substring(ts);
            else 
                tid = deflk.substring(ts,te);
            if(tid.length >= 1) {
                if(tid in this.species) {
                    this.push(strChunk);
                    this.fastaSections.accepted++;
                    acc = true; 
                    done();
                } 
            }
        } else { //check for species name
            let sp = deflk.indexOf(this.speciesNamePrefix); 
            if(sp !== -1) {
                 sp += this.speciesNamePrefix.length;
                 let se = deflk.indexOf(this.speciesNamePostfix,sp);
                 let tid = "";
                 if(se === -1)
                    tid = deflk.substring(sp);
                 else
                    tid = deflk.substring(sp,se);
                 if(tid.length >= 3) {
                     if(tid in this.species) {
                        this.push(strChunk);
                        this.fastaSections.accepted++;
                        acc = true;
                        done();
                     }
                 }
            }
        }
        if(!acc) {
            this.fastaSections.rejected++;
            done();
        }
    }  
    
    get filteringStats() {
        return this.fastaSections;
    }
}

module.exports=FASTATaxFilter;