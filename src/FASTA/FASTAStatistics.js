const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTAStatistics extends Transform {
    constructor(options,minLen=100,maxLen=100000000) {
        super(options);
        this._chunk = "";
        this.minLen = minLen;
        this.maxLen = maxLen;
        this.statistics = {
            aggr_len:[],
            char_counts:{},
            char_counts_by_section:{},
            section_length:{}
        };
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        let seq = felem.seq;
        this.statistics.aggr_len.push(seq.length);
        this.statistics.section_length[felem.def] = seq.length;
        this.statistics.char_counts_by_section[felem.def]={};
        while(true) {
            if(seq === "")
                break;
            let c = seq[0];
            let ct = seq.length;
            let re = new RegExp(c,'g');
            seq = seq.replace(re,"");
            ct = ct - seq.length;
            this.statistics.char_counts_by_section[felem.def][c]=ct;
            if(!(c in this.statistics.char_counts))
                this.statistics.char_counts[c]=0;
            this.statistics.char_counts[c] += ct;
        }
        done();
    }  
    calcAdditionalStats() {
        let self = this;
        return new Promise((resolve,reject) => {
            self.statistics["number_of_elements"]=self.statistics.aggr_len.length;
            let avg = 0;
            let min = self.statistics.aggr_len[0];
            let max = self.statistics.aggr_len[0];
            let sum = 0;
            for(let item of self.statistics.aggr_len) {
                sum += item;
                if(item < min) min = item;
                if(item > max) max = item;
            }
            avg = sum / self.statistics["number_of_elements"];
            self.statistics["avg_seq_len"] = avg;
            self.statistics["min_seq_len"] = min;
            self.statistics["max_seq_len"] = max;
            self.statistics["total_sequence_length"] = sum;
            resolve(true);
        });
    }
    writeStats(stream,type) {
        return new Promise((resolve,reject) => {
            this.calcAdditionalStats().then((ret) => {
                switch(type) {
                    case 'j': resolve(this.writeJSON(stream)); break;
                    case 't':resolve(this.writeText(stream)); break;
                    default: resolve(this.writeJSON(stream));
                }
            });
        });
    }
    
    writeJSON(stream) {
        return new Promise((resolve,reject) => {
            stream.write(JSON.stringify(this.statistics,null,4));
            resolve(true);
        });
    }
    
    writeText(stream) {
        return new Promise((resolve,reject) => {
            stream.write("Number of elements: " + this.statistics["number_of_elements"] + "\n");
            stream.write("Minimum sequence len: " + this.statistics["min_seq_len"] + "\n");
            stream.write("Average sequence length: " + this.statistics["avg_seq_len"] + "\n");
            stream.write("Maximum sequence length: " + this.statistics["max_seq_len"] + "\n");
            for(let c in this.statistics.char_counts) {
                stream.write(c + ": " + Math.round(this.statistics.char_counts[c]/this.statistics["total_sequence_length"]*100) + "%\n");
            }
            resolve(true);
        });
    }
}
module.exports=FASTAStatistics;