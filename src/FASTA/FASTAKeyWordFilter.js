const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

class FASTAKeyWordFilter extends Transform {
    constructor(options,keyword="protein") {
        super(options);
        this._chunk = "";
        this.keyword = keyword.toLowerCase();
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let felem = JSON.parse(strChunk);
        let deflk = felem.def.toLowerCase();
        if(deflk.includes(this.keyword))
            this.push(JSON.stringify(felem));
        done();
    }  
}
module.exports=FASTAKeyWordFilter;