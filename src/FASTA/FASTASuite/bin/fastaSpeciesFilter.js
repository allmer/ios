#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const TaxonomyBuilder = require("../../../helpers/TaxonomyBuilder");
const FASTAElement = require("../../FASTAElement");
const FASTATaxFilter = require("../../FASTATaxFilter");
const FASTAWriter = require("../../FASTAWriter");
const ProgressBar = require('progress');

let inFile = null;
let outFile = null;
let nodeFile = "nodes.dmp";
let nameFile = "names.dmp";
let showProgress = true;
let inStream=null; 
let outStream=null;
let highestTaxID = -1;
let taxPrefix = "TaxID=";
let taxPostfix = " ";
let specNamePrefix = "Tax=";
let specNamePostfix = " ";
let fastaseqlen = null;

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastaSpeciesFilter -i nr.fasta -taxID 39487 -o filtered.fasta");
    console.log("-i %FILE%\tinput file in FASTA format (required).");
    console.log("-o %FILE%\twhere output should be stored (required).");
    console.log("-p true|false\tshow progress bar (default: true).");
    console.log("-l number\tmax line length for FASTA file (default: none).");
    console.log("-t, --taxID %ID%\tThe highest taxid (https://www.ncbi.nlm.nih.gov/taxonomy) below which all species will be extracted from the input file (required).");
    console.log("-t1, --taxPrefix %STRING%\tA unique string preceeding the taxid in the definition line e.g., TaxID=; case sensitive (default: TaxID=).");
    console.log("-t2, --taxPostfix %STRING%\tA unique string following the taxid in the definition line e.g., ' ' (space); case sensitive (default: ' ').");
    console.log("-s1, --specNamePrefix %STRING%\tA unique string preceeding the species name in the definition line e.g., Tax=; case sensitive (default: Tax=).");
    console.log("-s2, --specNamePostfix %STRING%\tA unique string following the species name in the definition line e.g., ' ' (space); case sensitive (default: ' ').");
    console.log("-no %nodes.dmp%\tFile availabe for download from https://ftp.ncbi.nih.gov/pub/taxonomy/ within the taxcat archives (default looks in current folder).");
    console.log("-na %names.dmp%\tFile availabe for download from https://ftp.ncbi.nih.gov/pub/taxonomy/ within the taxcat archives (default looks in current folder).");
    console.log("--------------------------------");
    process.exit();
}

if(process.argv.length <= 4)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;
        case '-p':  i++;
                    let tmp = inArgs[i].trim().toLowerCase();
                    switch(tmp) {
                        case '1':
                        case 't':
                        case 'true':
                            showProgress = true;
                            break;
                        default: showProgress = false;
                    }
                    break;
        case '-l':  i++;
                    fastaseqlen = inArgs[i];
                    break;
        case '-no':  i++;
                    nodeFile = inArgs[i];
                    break;
        case '-na':  i++;
                    nameFile = inArgs[i];
                    break;
        case '-t':
        case '--taxID': i++;
                    highestTaxID = inArgs[i];
                    break;    
        case '-t1':
        case '--taxPrefix': i++;
                    taxPrefix = inArgs[i];
                    break;  
        case '-t2':
        case '--taxPostfix': i++;
                    taxPostfix = inArgs[i];
                    break;  
        case '-s1':
        case '--specNamePrefix': i++;
                    specNamePrefix = inArgs[i];
                    break;  
        case '-s2':
        case '--specNamePostfix': i++;
                    specNamePostfix = inArgs[i];
                    break;  
        default:    usage("Unknown switch: " + inArgs[i]);
    }
}

if((inFile !== null) && (inFile.length > 3)) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): ",err);
    });
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured during writing ("+outFile+"): ",err);
    });
}

if(nodeFile === null || nameFile === null)
    usage("nodes.dmp or names.dmp file not provided.");
fs.access(nodeFile, (err) => {if(err) usage("nodes.dmp file not found at "+nodeFile+".");});
fs.access(nameFile, (err) => {if(err) usage("names.dmp file not found at "+nameFile+".");});

if(highestTaxID === -1) {
    usage("the highest Taxonomy ID, for which all subordinate species "+
          "will be extracted from the input file, needs to be provided.");
}

let bar=null;
let taxbar = null;
if(showProgress) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  filtering [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });
    taxbar = new ProgressBar('  parsing taxonomy [:bar] :percent :etas', {
        complete: '=',
        incomplete: ' ',
        width: 40,
        total: 100
      });
  }
let tb = new TaxonomyBuilder(null);
tb.parseFiles(nameFile,nodeFile,taxbar).then((tax) => {
    tb.getTaxIDsUnder(highestTaxID).then((species) => {
        let fe = new FASTAElement(null,bar);
        let tafi = new FASTATaxFilter(null,species,taxPrefix,taxPostfix,specNamePrefix,specNamePostfix);
        let fw = new FASTAWriter(null,fastaseqlen);

        pipeline(
                inStream,
                split2(),
                fe,
                tafi,
                fw,
                outStream,
                err => {
                    if(err) {
                        console.log("\nProcessing failed: ",err);
                    } else {
                        if(inStream !== null) inStream.destroy();
                        if(outStream !== null) outStream.destroy();
                        console.log('\nProcessing succeeded.');
                        console.log("Accepted",tafi.filteringStats.accepted,"sequences and rejected",tafi.filteringStats.rejected,"sequences.");
                    }
                }
        );
    });
});





