#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTAElement");
const FASTAStatistics = require("../../FASTAStatistics");
const ProgressBar = require('progress');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastastats -i example.fasta");
    console.log("-i %FILE%\tinput file in FASTA format (required).");
    console.log("-o %FILE%\tWhere to store the statistics (default: stdout).");
    console.log("-f %Format%\tj; JSON or t:text (default: j).");   
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null; 
let inFile = null;
let outFile = null;
let outStream = null;
let format = "j";
let silentMode = false;


if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;      
        case '-f':  i++;
                    switch(inArgs[i]) {
                        case 'j':
                        case 'J': format = 'j'; break;
                        case 't':
                        case 'T': format = 't'; break;
                        default: format = 'j';
                    }
                    break;   
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;    
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    usage("You need to provide an input file with the -i switch.");
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured with the destination file provided ("+outFile+"): "+err);
    });
} else {
    outStream = process.stdout;
}

let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });      
  }
  
if(!silentMode) console.log("Setting up process.");  

let fe = new FASTAElement(null,bar);
let fst = new FASTAStatistics(null);

pipeline(
        inStream,
        split2(),
        fe,
        fst,
        (err) => {
            if(err) {
                usage(err);   
            } else {
                fst.writeStats(outStream,format);
                if(!silentMode) console.log("Done."); 
            }
        }
);
