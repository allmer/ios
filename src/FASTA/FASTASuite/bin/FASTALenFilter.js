#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTAElement");
const FASTALenFilter = require("../../FASTALenFilter");
const FASTAWriter = require("../../FASTAWriter");
const ProgressBar = require('progress');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastlenfilter -i example.fasta -o filtered.fasta -s 80 -h 150 //hairpins");
    console.log("-i %FILE%\tinput file in FASTA format (required).");
    console.log("-o %FILE%\tWhere to send the filtered file (default stdout).");
    console.log("-s %NUMBER%\tMin sequence length (inclusive, default: 125).");  
    console.log("-h %NUMBER%\tMax sequence length -1:unlimited (inclusive, default: 100.000.000).");  
    console.log("-l number\tmax line length for FASTA file; -1 for all on one line (default: 80).");  
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null; 
let outStream = null;
let inFile = null;
let outFile = null;
let silentMode = false;
let minLen = 125; 
let maxLen = 100000000;
let seqLineLen = 80;


if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;     
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;    
        case '-h':  i++;
                    maxLen = Number(inArgs[i]);
                    break;
        case '-s':  i++;
                    minLen = Number(inArgs[i]);
                    break;   
        case '-l':  i++;
                    seqLineLen = Number(inArgs[i]);
                    break; 
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    usage("You need to provide an input file with the -i switch.");
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured with the destination file provided ("+outFile+"): "+err);
    });
} else {
    outStream = process.stdout;
}


let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });    
  }
  
if(!silentMode) console.log("Setting up process.");  

let fe = new FASTAElement(null,bar);
let filter = new FASTALenFilter(null,minLen,maxLen);
let fw = new FASTAWriter(null,seqLineLen);

pipeline(
        inStream,
        split2(),
        fe,
        filter,
        fw,
        outStream,
        (err) => {
            if(err) {
                usage(err);   
            } else {
                if(!silentMode) console.log("Done."); 
            }
        }
);
