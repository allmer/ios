#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTAElement");
const FASTARename = require("../../FASTARename");
const FASTAWriter = require("../../FASTAWriter");
const ProgressBar = require('progress');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastarenamer -i example.fasta -o renamed.fasta");
    console.log("-i %FILE%\tinput file in FASTA format (required).");
    console.log("-o %FILE%\tWhere to store the file with renamed definition lines (dl) (default stdout).");
    console.log("-c %FILE%\tWhere to store the mapping file containing old dl and new dl).");
    console.log("-r %FILE%\ta reference for replacing definition lines with new ones (optional).");
    console.log("-d %CHAR/STRING%\thow to split the reference into current definition line and new definition line (default:|).");
    console.log("-p %STRING%\tprefix to prepend when using numbering (optional).");
    console.log("-s %FILE%\tsuffix to append when using numbering (optional).");
    console.log("-f %NUMBER%\tStart numbering at this number (default: 1).");  
    console.log("-l number\tmax line length for FASTA file; -1 for all on one line (default: 80).");  
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null; 
let mapStream = null;
let outStream = null;
let inFile = null;
let outFile = null;
let refFile = null;
let mapFile = null;
let silentMode = false;
let prefix = null;
let suffix = null;
let startAt = 1; 
let seqLineLen = 80;
let divider = "|";


if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;   
        case '-c':  i++;
                    mapFile = inArgs[i];
                    break;   
        case '-r':  i++;
                    refFile = inArgs[i];
                    break;  
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;    
        case '-f':  i++;
                    startAt = Number(inArgs[i]);
                    break;   
        case '-l':  i++;
                    seqLineLen = Number(inArgs[i]);
                    break; 
        case '-p':  i++;
                    prefix = inArgs[i];
                    break;  
        case '-d':  i++;
                    divider = inArgs[i];
                    break;  
        case '-s':  i++;
                    suffix = inArgs[i];
                    break; 
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    usage("You need to provide an input file with the -i switch.");
}

if(mapFile !== null) {
    mapStream = fs.createWriteStream(mapFile);
    mapStream.on('error', function(err) {
        usage("An error occured with the destination file provided ("+mapFile+"): "+err);
    });
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured with the destination file provided ("+outFile+"): "+err);
    });
} else {
    outStream = process.stdout;
}


let bar=null;
let refbar=null;
let mapbar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });
    refbar = new ProgressBar('  importing reference [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: 100
      });     
    mapbar = new ProgressBar('  writing new mappings to file [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: 100
      });      
  }
  
if(!silentMode) console.log("Setting up process.");  

let fe = new FASTAElement(null,bar);
let renamer = new FASTARename(null,startAt,prefix,suffix);
let fw = new FASTAWriter(null,seqLineLen);

function writeMap() {
    if(!silentMode) mapbar.total=Object.keys(renamer.mapping).length;
    mapStream.write("new"+divider+"old\n");
    for(let key in renamer.mapping) {
        mapStream.write(key);
        mapStream.write(divider);
        mapStream.write(renamer.mapping[key]);
        mapStream.write("\n");
        if(!silentMode) mapbar.tick(1);
    }
}

if(refFile !== null) {
    renamer.setMapping(refFile,refbar,divider).then((res)=>{
        pipeline(
                inStream,
                split2(),
                fe,
                renamer,
                fw,
                outStream,
                (err) => {
                    if(err) {
                        usage(err);   
                    } else {
                        if(mapStream !== null) writeMap();
                        if(!silentMode) console.log("Done."); 
                    }
                }
        );        
    }).catch((err)=>{
        usage(err);
    });
    
} else {
    pipeline(
            inStream,
            split2(),
            fe,
            renamer,
            fw,
            outStream,
            (err) => {
                if(err) {
                    usage(err);   
                } else {
                    if(mapStream !== null) writeMap();
                    if(!silentMode) console.log("Done."); 
                }
            }
    );
}
