#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTAElement");
const FASTAkmers = require("../../FASTAkmers");
const KMERWriter = require("../../KMERWriter");
const ProgressBar = require('progress');


function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastachopper -i example.fasta -o filtered.fasta -s 80 -h 150 //hairpins");
    console.log("-i %FILE%\tinput file in FASTA format (default: stdin).");
    console.log("-o %FILE%\tWhere to store the chopped sequences (default: stdout).");
    console.log("-k %NUMBER%\tK-mer length (default: 20).");  
    console.log("-s %NUMBER%\tK-mer overlap (default: 5).");  
    console.log("-l number\tmax line length for FASTA file; -1 for all on one line (default: 80).");  
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}


let inFile = null;
let outFile = null;
let inStream=null; 
let outStream=null;
let seqLineLen = 80;
let kLen = 20;
let overlap = 5;
let silentMode = false;

if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;     
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;    
        case '-k':  i++;
                    kLen = Number(inArgs[i]);
                    break;
        case '-s':  i++;
                    overlap = Number(inArgs[i]);
                    break;   
        case '-l':  i++;
                    seqLineLen = Number(inArgs[i]);
                    break; 
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if((inFile !== null) && fs.existsSync(inFile)) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        console.log("An error occured with reading the file provided (",inFile,"): ",err);
        process.exit();
    });
} else
    inStream = process.stdin;

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        console.log("An error occured during writing (",outFile,"): ",err);
        process.exit();
    });
} else
    outStream = process.stdout;

if(inStream === null) {
    usage("Input cannot be accessed.");
}
if(outStream === null) {
    usage("Output cannot be accessed.");
}

if(kLen <= 0)
    usage("k must be a positive number.");
if(overlap <=0)
    usage("The overlap must be 0 or a positive integer.");
if(kLen < overlap)
    usage("k must be larger than the desired overlap.");
if(seqLineLen < 1) 
    usage("The line length of the sequence in the FASTA file must be larger 0");

let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
    });    
}


let fe = new FASTAElement(null,bar);
let kmer = new FASTAkmers(null,kLen,overlap);
let kw = new KMERWriter(null,seqLineLen);
  
if(!silentMode) console.log("Processing started.");
pipeline(
        inStream,
        split2(),
        fe,
        kmer,
        kw,
        outStream,
        err => {
            if(err) {
               usage("Processing failed: ",err);
            } else {
                if(inStream !== null) inStream.destroy();
                if(outStream !== null) outStream.destroy();
                if(!silentMode) console.log('Processing succeeded.');
            }
        }
);





