#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const BlastScoreFilter = require("../../../blast/BlastScoreFilter");
const FASTAAnnotate = require("../../FASTAAnnotate");
const FASTAWriter = require("../../FASTAWriter");
const ProgressBar = require('progress');

/** Assumptions
 * input comes from blastfilter
**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastaannotate -q queries.fasta -o annotatedQueries.fasta -c mapping.tab");
    console.log("-i %FILE%||stdin\tblast output (default: stdin)."); 
    console.log("-q %FILE%\t file with the queries submitted to blast (required).");     
    console.log("-o %FILE%\tWhere to store the annotated fasta file (default: stdout).");
    console.log("-r %FILE%\tA file containing previous annotations (id|id|id.,...)");
    console.log("-c %FILE%\tWhere to store the new mapping file with matches references and blast results (required).");
    console.log("-d %CHAR/STRING%\thow to split the reference into current definition line and new definition line (default:|)."); 
    console.log("-l number\tmax line length for FASTA file; -1 for all on one line (default: 80).");  
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null;
let mapStream = null;
let outStream = null;
let refStream = null;
let inFile = null;
let outFile = null;
let mapFile = null;
let refFile = null;
let queryFile = null;
let silentMode = false;
let seqLineLen = 80;
let divider = "|";


if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;  
        case '-r':  i++;
                    refFile = inArgs[i];
                    break;  
        case '-c':  i++;
                    mapFile = inArgs[i];
                    break;  
        case '-q':  i++;
                    queryFile = inArgs[i];
                    break;  
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;     
        case '-d':  i++;
                    divider = inArgs[i];
                    break;  
        case '-l':  i++;
                    seqLineLen = Number(inArgs[i]);
                    break; 
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    inStream = process.stdin;
}

if(refFile !== null) {
    refStream = fs.createReadStream(refFile);
    refStream.on('error', function(err) {
        usage("An error occured with reading the reference file provided ("+refFile+"): "+err);
    });
} 

if(mapFile !== null) {
    mapStream = fs.createWriteStream(mapFile);
    mapStream.on('error', function(err) {
        usage("An error occured with the mapping file provided ("+mapFile+"): "+err);
    });
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured with the destination file provided ("+outFile+"): "+err);
    });
} else {
    outStream = process.stdout;
}


let bar=null;
let querybar=null;
let inmapbar=null;
let outmapbar=null;
if(!silentMode) {
    let inFileSize = 100;
    if(inStream !== process.stdin)
        inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });
    querybar = new ProgressBar('  importing queries [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: 100
      });     
    inmapbar = new ProgressBar('  importing references [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: 100
      });    
    outmapbar = new ProgressBar('  writing id mappings to file [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: 100
      });   
  }
  
if(!silentMode) console.log("Setting up process.");  

let bsf = new BlastScoreFilter(null,0.4);
let fa = new FASTAAnnotate(null);
let fw = new FASTAWriter(null,seqLineLen);

function writeMap() {
    if(!silentMode) outmapbar.total=Object.keys(fa.mapping).length;
    for(let key in fa.mapping) {
        mapStream.write(key);
        mapStream.write(divider);
        mapStream.write(fa.mapping[key].join(divider));
        mapStream.write("\n");
        if(!silentMode) outmapbar.tick(1);
    }
}

if(queryFile === null)
    usage("Providing the query file is required. ");
if(!fs.existsSync(queryFile))
    usage("Providing the query file is required. ");
if(refFile !== null && fs.existsSync(refFile)) {
    fa.setMapping(refFile,inmapbar,divider).then((val)=>{
        if(val === true) {
            fa.setQueries(queryFile,querybar).then((val) => {
                if(!silentMode && inStream === process.stdin) console.log("Processing input from blast via stdin, duration unknown."); 
                if(val === true) {
                    pipeline(
                            inStream,
                            split2(),
                            //bsf,
                            fa,
                            fw,
                            outStream,
                            (err) => {
                                if(err) {
                                    usage(err);   
                                } else {
                                    //console.log("1",fa.mapping);
                                    if(mapStream !== null) writeMap();
                                    if(!silentMode) console.log("Done."); 
                                }
                            }
                    );                    
                }
            });
        }
    });
} else {
    fa.setQueries(queryFile,querybar).then((val) => {
         if(val === true) {
             pipeline(
                     inStream,
                     split2(),
                     bsf,
                     fa,
                     fw,
                     outStream,
                     (err) => {
                         if(err) {
                             usage(err);   
                         } else {
                            //console.log("2",fa.mapping);
                            if(mapStream !== null) writeMap();
                            if(!silentMode) console.log("Done."); 
                         }
                     }
             );                    
         }
     });    
}

//blastx -query 00010.fasta -db /scratch/hpc-prf-puccmirs/references/liliopsidaUniRef.fasta -outfmt "6 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore" | fastaannotate -q 00010.fasta -c mapping.tab -o annot_00010.fasta
