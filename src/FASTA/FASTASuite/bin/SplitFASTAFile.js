#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTAElement");
const FASTASplitter = require("../../FASTASplitter");
const ProgressBar = require('progress');

/** Assumptions
 * input comes from FASTAElement
 * Or has following format: {'def':'Definition line (starting with >)','seq':'ACGTTTGTA... no whitespace'}
**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: fastasplitter -i example.fasta -b part%i.fa -m 500");
    console.log("-i %FILE%\tinput file in FASTA format (required).");
    console.log("-b %base name%\tWhere to store the split files (default split%i.fasta) where %i will be replaced by a number.");
    console.log("-m %NUMBER%\tNumber of FASTA sections per file (default: 1000).");  
    console.log("-l number\tmax line length for FASTA file; -1 for all on one line (default: 80).");  
    console.log("-n [true||false]\tSilent mode (default: false).");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null; 
let inFile = null;
let baseName = null;
let silentMode = false;
let seqLineLen = 80;
let maxQueries = 1000;


if(process.argv.length < 3)
    usage("Review the required arguments below.");
var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;   
        case '-b':  i++;
                    baseName = inArgs[i];
                    break;     
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;    
        case '-m':  i++;
                    maxQueries = Number(inArgs[i]);
                    break;   
        case '-l':  i++;
                    seqLineLen = Number(inArgs[i]);
                    break; 
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    usage("You need to provide an input file with the -i switch.");
}


let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  processing [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });      
  }
  
if(!silentMode) console.log("Setting up process.");  

let fe = new FASTAElement(null,bar);
let splitter = new FASTASplitter(null,baseName,maxQueries,seqLineLen);

pipeline(
        inStream,
        split2(),
        fe,
        splitter,
        (err) => {
            if(err) {
                usage(err);   
            } else {
                if(!silentMode) console.log("Done."); 
            }
        }
);
