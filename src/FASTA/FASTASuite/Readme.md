# Purpose
The aim of this set of tools is to allow handling FASTA formatted files in an efficient manner. All operations are stream based and typically the file system is the speed bottleneck. 

All underlying stream transformations can be used to piece together your own FASTA processing pipelines and are available in this package.

# Installation
>`npm install -g fastasuite`

>>-g so that you can run the tools directly from the terminal.

# Tools and Transforms
FASTA format is relatively simple. Files in that format tend to become very large quickly and cannot be handled by many text viewers. Other difficult to consolidate tasks include 
ensuring that definition lines are unique for one file. When combining multiple FASTA files (not supported by fastasuite: Linux: cat *.fasta > combined.fa; Windows copy *.fasta combined.fa) 
the definition lines may not be unique anymore. fastarenamer can solve that problem before or after combining the FASTA files.

The expected input is typically a FASTA formatted file or stream with such data:\
>\>definition line (no special formatting considered)\
SEQUENCE IN AA OR NT FORMAT\
CONTINUATION OF SEQUENCE\
\>definition line of next FASTA SECTION\
NEXT SEQUENCE

Input is handled by the FASTAElement transform which provides one FASTA section in JSON format to the next handler in the stream pipeline.
The format is `{'def':'xxx','seq':'yyy'}` \
Many other transforms expect this format as input and will provide the same output (e.g., FASTAKeyWordFilter, FASTALenFilter, ...).

The FASTAWriter is typically the terminal point for FASTA handling pipelines and it writes the information in the JSON object in FASTA format. Since some tools may expect sequences to be split at regular intervals, this can be achieved by setting such an interval: `let fw = new FASTAWriter(null,80);` 

## Available Tools
All tools have accompanying transforms which can be pieced together to form various processing pipelines. Any of the tools can be run without arguments to get usage instructions.

>fastadeflineextractor\
fastarenamer\
fastaspeciesfilter\
fastalenfilter\
fastakeywordfilter\
fastachopper\
tbc.


**Example:**

>`fastaSpeciesFilter -i nr.fasta -o filtered.fasta -no nodes.dmp -na names.dmp -t 10090`

This will result in the extraction of all headers containing mouse or a subspecies of mouse. 

**nodes.dmp** and **names.dmp** are files available at [NCBI](https://www.ncbi.nlm.nih.gov/taxonomy). These files need to be downloaded from NCBI and provided as input. If they are not specified with -na or -no, they are assumed to exist in the current directory. The taxonomy IDs are also available from NCBI. 

## Support
You can submit errors or feature requests here: [https://bitbucket.org/allmer/ios/src/master/](https://https://bitbucket.org/allmer/ios/src/master/)\
Feel free to contribute (explanations, code, tests, etc.). Become a member of Bitbucket and create an issue telling us your user name and that you would like to contribute.