const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');

/** Assumptions
 * input must be in FASTA format
**/

class FASTAElement extends Transform {
    constructor(options,bar = null) {
        super(options);
        this._chunk = "";
        this.felem={'def':'','seq':''};
        this.bar = bar; //https://www.npmjs.com/package/progress 
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003') {
            console.log(this.felem.def);
            this.push(JSON.stringify(this.felem));
            if(this.bar !== null) bar.terminate();
            process.exit();
        }
        if(this.bar !== null) this.bar.tick(chunk.length+1); //line endings were removed previously.
        if(strChunk.startsWith(">")) {
            if(this.felem.seq.length>1) {
                this.push(JSON.stringify(this.felem));
            }
            this.felem = {};
            this.felem['def'] = strChunk;
            this.felem['seq'] = "";
            done();
        } else {
            this.felem['seq'] += strChunk.trim();
            done();
        }
    }
    
    _flush = function(done) {
        this.push(JSON.stringify(this.felem));
        done();
    }
}

module.exports=FASTAElement;