const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTA/FASTAElement");
const FASTASeqType = require("../../FASTA/FASTASeqType");
const FASTAWriter = require("../../FASTA/FASTAWriter");
const assert = require("assert");
const chai = require('chai');
const expect = chai.expect;

describe('Test FASTA seq type check', function() {
    it('Should assign the right type (nucleotide).', function(done) {
        this.timeout(5000);
        let fe = new FASTAElement(null);
        let fst = new FASTASeqType(null);
        let inStream = fs.createReadStream("../files/filtered.fasta");
        let outStream = fs.createWriteStream("../files/filtered_typed.fasta");
        let fw = new FASTAWriter(null,80);        
        pipeline(
                inStream,
                split2(),
                fe,
                fst,
                fw,
                outStream,
                err => {
                    if(err) {
                        assert(1,2);
                        done();
                    } else {
                        outStream.destroy();
                        fs.unlink("../files/filtered_typed.fasta",(err)=>{if(err)console.log(err);});
                        assert(fst.type,"nucleotide");
                        done();
                    }
                }
        );        
    });
    it('Should assign the right type (aminoacid).', function(done) {
        this.timeout(5000);
        let fe = new FASTAElement(null);
        let fst = new FASTASeqType(null);
        let inStream = fs.createReadStream("../files/prot.fasta");
        let outStream = fs.createWriteStream("../files/prot_typed.fasta");
        let fw = new FASTAWriter(null,80);        
        pipeline(
                inStream,
                split2(),
                fe,
                fst,
                fw,
                outStream,
                err => {
                    if(err) {
                        assert(1,2);
                        done();
                    } else {
                        outStream.destroy();
                        fs.unlink("../files/prot_typed.fasta",(err)=>{if(err)console.log(err);});
                        assert(fst.type,"aminoacid");
                        done();
                    }
                }
        );        
    }); 
    it('Should throw an error.', function(done) {
        this.timeout(5000);
            let fe = new FASTAElement(null).on('error', function(err) {
                console.log("fe",err);
            });
            let fst = new FASTASeqType(null).on('error', function(err) {
                console.log("fst",err);
            });
            let inStream = fs.createReadStream("../files/bad.fasta").on('error', function(err) {
                console.log("inStream",err);
            });
            let outStream = fs.createWriteStream("../files/bad_typed.fasta").on('error', function(err) {
                console.log("outStream",err);
            });
            let fw = new FASTAWriter(null,80).on('error', function(err) {
                console.log("fw",err);
            });  
            let sp = new split2().on('error', function(err) {
                console.log("sp",err);
            });  
            function pip() {
                pipeline(
                    inStream,
                    sp,
                    fe,
                    fst,
                    fw,
                    outStream,
                    err => {
                        if(err) {
                            console.log("pipe",err);
                            done();
                        } else {
                            console.log("good pipe");
                            assert("true",false);
                            done();
                        }
                    }
                );
            }            
            expect(
                    pip
        ).to.throw("UNEXPECTED CHARACTER");
    });     
});
