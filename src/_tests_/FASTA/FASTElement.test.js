const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const TaxonomyBuilder = require("../../helpers/TaxonomyBuilder");
const FASTAElement = require("../../FASTA/FASTAElement");
const FASTATaxFilter = require("../../FASTA/FASTATaxFilter");
const FASTAWriter = require("../../FASTA/FASTAWriter");
const assert = require("assert");

describe('Test FASTA Element', function() {
    it('Should read a fasta file and provide a FASTAElement at a time.', function(done) {
        this.timeout(5000);
        
    });
});
