const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const TaxonomyBuilder = require("../../helpers/TaxonomyBuilder");
const FASTAElement = require("../../FASTA/FASTAElement");
const FASTATaxFilter = require("../../FASTA/FASTATaxFilter");
const FASTAWriter = require("../../FASTA/FASTAWriter");
const assert = require("assert");

describe('Test fasta species filter', function() {
    it('Should create a file with some content.', function(done) {
        this.timeout(60000);
        let tb = new TaxonomyBuilder(null,"../../files/names.dmp","../../files/nodes.dmp");
        tb.parseFiles().then((tax) => {
            console.log("Taxonomy built.");
            tb.getTaxIDsUnder(39107).then((species) => {
                console.log(Object.keys(species).length);
                let inFile = "../files/unirefsample.fasta";
                if(!fs.existsSync(inFile))
                    console.log("prolem with file");
                let inStream = fs.createReadStream(inFile);
                let outStream = fs.createWriteStream("39107_unireffiltered.fasta");
                let fe = new FASTAElement(null);
                let tafi = new FASTATaxFilter(null,species);
                let fw = new FASTAWriter(null);
                console.log("Filtering");
                pipeline(
                        inStream,
                        split2(),
                        fe,
                        tafi,
                        fw,
                        outStream,
                        err => {
                            if(err) {
                                console.log(err);
                                assert.equal(0,1);
                                done();
                            } else {
                                if(inStream !== null) inStream.destroy();
                                if(outStream !== null) outStream.destroy();
                                console.log('Processing succeeded.');
                                done();
                            }
                        }
                );
            });
        });
    });
});
