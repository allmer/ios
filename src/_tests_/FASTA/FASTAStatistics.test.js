const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const FASTAElement = require("../../FASTA/FASTAElement");
const FASTAStatistics = require("../../FASTA/FASTAStatistics");
const assert = require("assert");
const chai = require('chai');
const expect = chai.expect;

describe('Test FASTA statistics nucletotide count', function() {
    it('Should assign the right number for nucleotides for the first section.', function(done) {
        this.timeout(5000);
        let fe = new FASTAElement(null);
        let fst = new FASTAStatistics(null);
        let inStream = fs.createReadStream("../files/00010.fasta");    
        pipeline(
                inStream,
                split2(),
                fe,
                fst,
                err => {
                    if(err) {
                        assert(1,2);
                        done();
                    } else {
                        fst.calcAdditionalStats();
                        //console.log(fst.statistics);
                        assert(fst.statistics.char_counts_by_section[">pdi2001denovo"]['A'],343);
                        assert(fst.statistics.char_counts_by_section[">pdi2001denovo"]['C'],220);
                        assert(fst.statistics.char_counts_by_section[">pdi2001denovo"]['G'],164);
                        assert(fst.statistics.char_counts_by_section[">pdi2001denovo"]['T'],397);
                        done();
                    }
                }
        );        
    });
    it('Should count sequence length correctly', function(done) {
        this.timeout(5000);
        let fe = new FASTAElement(null);
        let fst = new FASTAStatistics(null);
        let inStream = fs.createReadStream("../files/00010.fasta");    
        pipeline(
                inStream,
                split2(),
                fe,
                fst,
                err => {
                    if(err) {
                        assert(1,2);
                        done();
                    } else {
                        assert(fst.statistics.aggr_len[0],1124);
                        assert(fst.statistics.aggr_len[1],610);
                        assert(fst.statistics.section_length['>pdi2004denovo'],1570);
                        done();
                    }
                }
        );        
    });

    it('Should store text file', function(done) {
        this.timeout(5000);
        let fe = new FASTAElement(null);
        let fst = new FASTAStatistics(null);
        let inStream = fs.createReadStream("../files/00010.fasta");   
        let outStream = fs.createWriteStream("../files/0010.stats.txt");
        pipeline(
                inStream,
                split2(),
                fe,
                fst,
                err => {
                    if(err) {
                        assert(1,2);
                        done();
                    } else {
                        fst.writeStats(outStream,'t').then((ret)=>{
                            const dta = fs.readFileSync("../files/0010.stats.txt", {encoding:'utf8', flag:'r'}).split("\n");
                            assert(dta[4],"A: 26%");
                            done();
                        });
                    }
                }
        );        
    });    
});
