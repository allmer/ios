const TaxonomyBuilder = require("../../helpers/TaxonomyBuilder.js");
const assert = require("assert");

describe('TaxonomyBuilder', function() {
//    it('Should Build and find taxid 1815267.', function(done) {
//        this.timeout(18000); //takes roughly 8s perhaps future tests could use a subset of the complete taxonomy.
//        let tb = new TaxonomyBuilder();
//        tb.parseFiles().then((tax)=>{
//            assert.equal(tax.getTaxonomy()[1815267].parentID,1815265);
//            assert.equal(tax.getTaxonomy()[1815267].rank,'species');
//            assert.equal(tax.getTaxonomy()[1815267].taxID,1815267);
//            assert.equal(tax.getTaxonomy()[1815267].name,'Amphilimna polyacantha');
//            done();     
//        }).catch(err => {
//            console.log(err);
//            assert.equal(true,false);
//        });
//    });
//    it('Should Build and find taxid 1815267 as parent id.', function(done) {
//        this.timeout(18000); //takes roughly 8s perhaps future tests could use a subset of the complete taxonomy.
//        let tb = new TaxonomyBuilder();
//        tb.parseFiles().then((tax)=>{
//            let tst = tax.getTaxonomy('par')[1815267];
//            assert.strictEqual(tst,undefined);
//            assert.equal(tax.getTaxonomy('par')[tax.getTaxonomy()[1815267].parentID].children[1815267],'species');
//            assert.equal(tax.getTaxonomy('par')[tax.getTaxonomy()[1815267].parentID].children[2566087],'species');
//            done();     
//        }).catch(err => {
//            console.log(err);
//            assert.equal(true,false);
//        });
//    });  
    it('Should extract the children of 1815265.', function(done) {
        this.timeout(30000); //takes roughly 11s perhaps future tests could use a subset of the complete taxonomy.
        let tb = new TaxonomyBuilder();
        tb.parseFiles().then((tax)=>{
            tb.getTaxIDsUnder(tb.getTaxonomy('dir')[1815267].parentID).then((children) => {
                //console.log(children);
                assert.equal(children[1815267],'Amphilimna polyacantha');
                assert.equal(children['Amphilimna polyacantha'],1815267);
                assert.equal(children[2566087],'Amphilimna granulosa');
                assert.equal(children['Amphilimna granulosa'],2566087);
                done();
            });     
        }).catch(err => {
            console.log(err);
            assert.equal(true,false);
        });
    });      
});
