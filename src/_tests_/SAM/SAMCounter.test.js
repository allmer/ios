const SAMCounter = require("../../SAM/SAMCounter.js");
const {pipeline} = require('stream');
const fs = require('fs');
const split2 = require('split2');
const assert = require("assert");

describe('SAMCounter', function() {
    it('Should count occurence of alignment flags.', function(done) {
        let sc = new SAMCounter();
        let inStream = fs.createReadStream("../files/SAMSample.sam");
        pipeline(
                inStream,
                split2(),
                sc,
                err => {
                    if(err) {
                        console.log("Test failed: ",err);
                        assert.equal(true,false);
                        done();
                    } else {
                        assert.equal(sc.getFlagCount(161),120);
                        assert.equal(sc.getFlagCount(97),126);
                        assert.equal(sc.getFlagCount(153),1);
                        done();
                    }
                }
        );
    });
});
