const SAMUnmappedFilter = require("../../SAM/SAMUnmappedFilter");
const {pipeline} = require('stream');
const fs = require('fs');
const split2 = require('split2');
const assert = require("assert");

describe('SAMUnmappedFilter', function() {
    it('Should create a left an right file with same ids.', function(done) {
        
        let inStream = fs.createReadStream("../files/SAMSample.sam");
        let ufilter = new SAMUnmappedFilter(null,true);
        let left = fs.createWriteStream("./left.test");
        let right = fs.createWriteStream("./right.test");
        ufilter.listenLeft(left);
        ufilter.listenRight(right);
        pipeline(
                inStream,
                split2(),
                ufilter,
                err => {
                    if(err) {
                        console.log("Test failed: ",err);
                        assert.equal(true,false);
                        done();
                    } else {
                        let ldata = fs.readFileSync("./left.test",{encoding:'utf8', flag:'r'}).split("\n");
                        let rdata = fs.readFileSync("./right.test",{encoding:'utf8', flag:'r'}).split("\n");
                        fs.unlink("./left.test",(err) => {
                            if(err) console.log("file not deleted: ",err);
                        });
                        fs.unlink("./right.test",(err) => {
                            if(err) console.log("file not deleted: ",err);
                        });  
                        for(let i=0; i<ldata.length; i+=4) {
                            let l = ldata[i].split("/")[0];
                            let r = rdata[i].split("/")[0];
                            assert.equal(l,r);
                        }
                        done();
                    }
                }
        );
    });
});
