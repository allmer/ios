const conv = require("../../SAM/SAMConversions.js");
const assert = require("assert");

describe('SAM Conversions', function() {
    describe('Decimal to binary conversion', function() {
        it('Should produce a binary with 1 position.', function() {
           assert.equal(conv.d2b(1).length,1);
        });
        it('Should produce a binary with 12 position.', function() {
           assert.equal(conv.d2b(4095).length,12);
        });    
        it('Should convert decimal 1226 to binary 10011001010.', function() {
           assert.equal(conv.d2b(1226),"10011001010");
        });
    });
    describe('Binary string to SAMFlags array', function() {
        it('Should produce an arry with 12 positions.', function() {
           assert.equal(conv.flagArr(1).length,12);
        });
        it('Should have certain flags set.', function() {
            let farr = conv.flagArr('10011001010'); //not allowed flag combination; but is conversion proper?
            assert.equal(farr[0],false);
            assert.equal(farr[1],true);
            assert.equal(farr[2],false);
            assert.equal(farr[3],true);
            assert.equal(farr[4],false);
            assert.equal(farr[5],false);
            assert.equal(farr[6],true);
            assert.equal(farr[7],true);
            assert.equal(farr[8],false);
            assert.equal(farr[9],false);
            assert.equal(farr[10],true);
            assert.equal(farr[11],false);
        });    
        it('Should have certain flags set.', function() {
            let farr = conv.flagArr(conv.d2b(133)); //unmapped
            assert.equal(farr[0],true);
            assert.equal(farr[1],false);
            assert.equal(farr[2],true);
            assert.equal(farr[3],false);
            assert.equal(farr[4],false);
            assert.equal(farr[5],false);
            assert.equal(farr[6],false);
            assert.equal(farr[7],true);
            assert.equal(farr[8],false);
            assert.equal(farr[9],false);
            assert.equal(farr[10],false);
            assert.equal(farr[11],false);
        });   
    });    
});
