const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('./SAMConversions.js');

/** Assumptions
 * input must be in SAM format
 * input must be name sorted
 * Test only done for Bowtie2 output

**/

class SAMFlagFilter extends Transform {
    constructor(options,flagArr,bar=null) {
        super(options);
        this._chunk = "";
        this.matepair = {};
        this.matepair["ID"] = "unset";
        this.matepair["propermap"] = true; //initialize to true so that the first ID (unset) is ignored in output.
        this.matepair["leftmap"] = true;
        this.matepair["rightmap"] = true;
        this.matepair["first"] = [0,1];
        this.matepair["second"] = [0,1];
        this.matepair["count"] = 0;
        this.flagArr = flagArr;
        this.singlet = null;
        this.left = null;
        this.right = null;
        this.interleaved = null;
        this.bar = bar;
    }
    
    _transform(chunk,encoding,done) {
        if(this.bar !== null) this.bar.tick(chunk.length +1);
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003') {
            this.singlet.push(null);
            this.left.push(null);
            this.right.push(null);
            this.interleaved.push(null);
            process.exit();
        }
        if(strChunk.startsWith("@")) {
            done();
        } else {
            //console.log(strChunk);
            let dta = strChunk.split("\t");
            let id = dta[0].trim();
            let decFlag = dta[1].trim();
            let foundFlags = conv.flagArr(conv.d2b(decFlag));
            //console.log(id,foundFlags);
            if(this.matepair.ID !== id) { //first ID or new ID encountered
                //console.log("new id",id);
                if(decFlag in this.flagArr) {  
                    //console.log(this.matepair);
                    if(this.matepair.singlet || !(this.matepair.first && this.matepair.second)) {
                        this.writeToFile(this.matepair.first,this.singlet);
                    } else {
                        if(this.interleaved !== null)
                            this.writeToFile(this.matepair,this.interleaved); //caution, different data sent
                        if(this.left !== null)
                            this.writeToFile(this.matepair.first,this.left);
                        if(this.right !== null)
                            this.writeToFile(this.matepair.second,this.right);
                    }
                } 
                 
                this.matepair = {};
                this.matepair["propermap"] = false; //set to false.
                this.matepair["ID"] = id;
                this.matepair["leftmap"] = false;
                this.matepair["rightmap"] = false;
                this.matepair["count"] = 1;
            } else {
                this.matepair.count++;
            }
            if(!foundFlags[0]) { //read not paired
                //console.log("not paired");
                this.matepair["singlet"] = true;
                this.matepair["first"] = dta;
                this.matepair["flags"] = foundFlags;
            } else { //read paired
                if(foundFlags[6]) { //first or left or /1
                    //console.log("left");
                    this.matepair["first"] = dta;
                    this.matepair["flags"] = foundFlags;
                    if(!foundFlags[2]) 
                        this.matepair["leftmap"] = true;
                }
                if(foundFlags[7]) { //second or right or /2
                    //console.log("right");
                    this.matepair["second"] = dta;
                    this.matepair["flags"] = foundFlags;
                    if(!foundFlags[2]) 
                        this.matepair["rightmap"] = true;
                }
                if(foundFlags[1]) { //at least one alignment for the ID is in a proper pair
                    this.matepair["propermap"] = true;
                }
            }
            done();
        }
    }

    writeToFile(data,stream) {
        if(this.singlet !== null && stream === this.singlet) {
            //console.log("Encountered singlet.");
            this.singlet.write("@" + data[0] + "\n");
            this.singlet.write(data[9] + "\n");
            this.singlet.write("+\n");
            this.singlet.write(data[10] + "\n");
        } else {
            if(this.left !== null && stream === this.left) {
                this.left.write("@" + data[0] + "/1\n");
                this.left.write(data[9] + "\n");
                this.left.write("+\n");
                this.left.write(data[10] + "\n");
            } else {
                if(this.right !== null && stream === this.right) {
                    this.right.write("@" + data[0] + "/2\n");
                    this.right.write(data[9] + "\n");
                    this.right.write("+\n");
                    this.right.write(data[10] + "\n");
                } else { //interleaved
                    if(this.interleaved !== null) {
                        //console.log(data.first[1],data.second[1]);
                        this.left.write("@" + data[0] + "/1\n");
                        this.left.write(data[9] + "\n");
                        this.left.write("+\n");
                        this.left.write(data[10] + "\n");
                        this.right.write("@" + data[0] + "/2\n");
                        this.right.write(data[9] + "\n");
                        this.right.write("+\n");
                        this.right.write(data[10] + "\n");
                    }
                }
            }
        }
    }
    listenLeft(destination) {
        this.left = destination;
    }
    listenRight(destination) {
        this.right = destination;
    }
    listenSinglet(destination) {
        this.singlet = destination;
    }
    listenInterleaved(destination) {
        this.interleaved = destination;
    }
}

module.exports=SAMFlagFilter;