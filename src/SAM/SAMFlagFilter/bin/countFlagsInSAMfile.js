#!/usr/bin/env node

const {pipeline} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('../lib/SAMConversions.js');
const SAMCounter = require("../lib/SAMCounter.js");
const ProgressBar = require('progress');

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: countsamflags -i example.sam -l 1000 -s true");
    console.log("-i %FILE%\tinput file in SAM format (required)");
    console.log("-o %FILE%\toutput file tab separated values (default: stdout)");
    console.log("-l %NUM%\thow many alignments too look at (default: -1 = all alignments)");
    console.log("-s [true||false] Whether to produce any output other than the tab separated results (default: false)");
    console.log("--------------------------------");
    process.exit();
}

let inFile = null;
let outFile = null;
let instream = null;
let outStream = null;
if(process.argv.length <= 3)
    usage("review the usage of this tool below.");
var inArgs = process.argv.slice(2);
let sampleMax = -1;
let silentMode = false;

for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;
        case '-o':  i++;
                    outFile = inArgs[i];
                    break;
        case '-l':  i++;
                    sampleMax = inArgs[i];
                    break;      
        case '-s':  i++;
                    let tmp = inArgs[i].trim().toLowerCase();
                    switch(tmp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;             
        default:    usage("Unknown switch: " + inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided (" + inFile + "): " + err);
    });
} else {
    usage("No input file provided.");
}

if(outFile !== null) {
    outStream = fs.createWriteStream(outFile);
    outStream.on('error', function(err) {
        usage("An error occured with writing to the file provided (" + outFile + "): " + err);
    });
}

function printTabSep(stream,samct,sampleMax,silentMode) {
    let sampling = (sampleMax > 0)?"sampling "+sampleMax+" alignments":"considering all alignments";
    if(!silentMode) console.log("The following alignment flags (", sampling ,") were found in the file (",inFile,"):");
    stream.write("flag\tcount\tread paired\tread mapped in proper pair\tread unmapped\tmate unmapped\tread reverse strand\tmate reverse strand\tfirst in pair\tsecond in pair\tnot primary alignment\tread fails platform/vendor quality checks\tread is PCR or optical duplicate\tsupplementary alignment\n");
    //console.log("flag\tcount\tread paired\tread mapped in proper pair\tread unmapped\tmate unmapped\tread reverse strand\tmate reverse strand\tfirst in pair\tsecond in pair\tnot primary alignment\tread fails platform/vendor quality checks\tread is PCR or optical duplicate\tsupplementary alignment");
    let farr = samct.getFlagCounts();
    for(let f=0; f<farr.length; f++) {
        if(farr[f]>0) { //an flag with count
            let bin = conv.d2b(f);
            let flags = conv.flagArr(bin);
            stream.write(f+"\t"+farr[f]+"\t"+flags.join('\t')+"\n");
            //console.log(f+"\t"+farr[f]+"\t"+flags.join('\t'));
        }
    }    
}


let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  counting [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });
  }
  
let sc = new SAMCounter(null,sampleMax,bar);
if(!silentMode) console.log("Starting Count.");
pipeline(
        inStream,
        split2(),
        sc,
        err => {
            if(err) {
                if(err === "NORMAL_TERMINATION_SET_LIMIT_REACHED") {//"Error [ERR_STREAM_PREMATURE_CLOSE]: Premature close") {
                    printTabSep(sc,sampleMax,silentMode);
                } else
                    usage("Processing failed: (" + err +")");
            } else {
                if(inStream !== null) inStream.destroy();
                if(outStream === null)
                    printTabSep(process.stdout,sc,sampleMax,silentMode);
                else {
                    printTabSep(outStream,sc,sampleMax,silentMode);
                    //outStream.destroy();
                }
            }
        }
);
