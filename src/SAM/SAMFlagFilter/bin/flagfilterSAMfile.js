#!/usr/bin/env node

const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('../lib/SAMConversions.js');
const SAMFlagFilter = require("../lib/SAMFlagFilter");
const ProgressBar = require('progress');

/** Assumptions
 * input must be in SAM format
 * input must be name sorted
 * Test only done for Bowtie2 output

**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: flagfiltersamfile -i example.sam -left 1.fq -f 97,145 -right r.fq");
    console.log("-i %FILE%\tinput file in SAM format");
    console.log("-l %FILE%\tWhere to store /1 reads");
    console.log("-r %FILE%\tWhere to store /2 reads");
    console.log("-m %FILE%\tWhere to store /1 and /2 reads in an interleaved file");
    console.log("-s %FILE%\tWhere to store singlet reads");
    console.log("-n [true||false]\tSilent mode");
    console.log("-f %num,num,...%\tThe flags that should be extracted as reads. If reads are paired flags must also be complementary. -f can be used multiple times and the flags are aggregated");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null, left = null, right = null, singlet = null, interleaved = null;
let inFile = null, leftFile = null, rightFile = null, singletFile = null, interleavedFile = null;
let silentMode = false;
//let mode = true; //Extractonly pairs where neither of the mates is ever aligned (given multimaps).
let flags = {}; //Extractonly pairs that are never properly aligned as a pair.

var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;
        case '-l':  i++;
                    leftFile = inArgs[i];
                    break;
        case '-r':  i++;
                    rightFile = inArgs[i];
                    break;
        case '-s':  i++;
                    singletFile = inArgs[i];
                    break;
        case '-m':  i++;
                    interleavedFile = inArgs[i];
                    break;    
        case '-n':  i++;
                    let tnp = inArgs[i].trim().toLowerCase();
                    switch(tnp) {
                        case '1':
                        case 't':
                        case 'true':
                            silentMode = true;
                            break;
                        default: silentMode = false;
                    }
                    break;   
        case '-f':  i++;
                    let tmp = inArgs[i].trim();
                    if(tmp.includes(",")) {
                        let arr = tmp.split(",");
                        for(let i=0; i<arr.length; i++)
                            flags[arr[i].toString()]=0;
                    } else 
                        flags[tmp.toString()]=0;
                    break;
         
        default:    usage("Unknown switch "+inArgs[i]);
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        usage("An error occured with reading the file provided ("+inFile+"): "+err);
    });
} else {
    usage("You need to provide an input file with the -i switch.");
}

if(leftFile !== null) {
    left = fs.createWriteStream(leftFile);
    left.on('error', function(err) {
        usage("An error occured with the left,/1,firts file provided ("+leftFile+"): "+err);
    });
}

if(rightFile !== null) {
    right = fs.createWriteStream(rightFile);
    right.on('error', function(err) {
        usage("An error occured with the right,/2,second file provided ("+rightFile+"): "+err);
    });
}

if(singletFile !== null) {
    singlet = fs.createWriteStream(singletFile);
    singlet.on('error', function(err) {
        usage("An error occured with the singleton file provided ("+singletFile+"): "+err);
});
}

if(interleavedFile !== null) {
    interleaved = fs.createWriteStream(interleavedFile);
    interleaved.on('error', function(err) {
        usage("An error occured with the interleaved file provided ("+interleavedFile+"): "+err);
});
}

if(interleaved === null && singlet === null && left === null && right === null)
    usage("No output file was provided.");

if((left === null && right !== null) || (left !== null && right === null))
    usage("Both, left and right file need to be provided for paired reads. Otherwise specify a singlet file.");

let bar=null;
if(!silentMode) {
    let inFileSize = fs.statSync(inFile).size;
    bar = new ProgressBar('  filtering [:bar] :rate/bps :percent :etas', {
        complete: '*',
        incomplete: ' ',
        width: 40,
        total: inFileSize
      });
  }
  
if(!silentMode) console.log("Setting up flag filter.");
if(!silentMode) console.log(flags);
let sf = new SAMFlagFilter(null,flags,bar);
if(interleaved !== null) sf.listenInterleaved(interleaved);
if(left !== null) sf.listenLeft(left);
if(right !== null) sf.listenRight(right);
if(singlet !== null) sf.listenSinglet(singlet);
if(!silentMode) console.log("Starting process.");
pipeline(
        inStream,
        split2(),
        sf,
        err => {
            if(err) {
                usage("Processing failed: "+err);
                if(inStream !== null) inStream.destroy();
                if(left !== null) left.destroy();
                if(right !== null) left.destroy();
                if(singlet !== null) left.destroy();
                if(interleaved !== null) left.destroy();
                if(!silentMode) console.log('Processing succeeded.');
            }
        }
);
