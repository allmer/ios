function dec2bin(x) {
        let bin = "";
        let rem, i = 1, step = 1;
        while (x !== 0) {
            rem = x % 2;
            x = parseInt(x / 2);
            bin = rem + bin;
            i = i * 10;
        }
        return bin;
    }
    
function binStringToSAMFlagsArray(binary) {
        let farr = new Array(12);
        let pos = 12, a=0;
        for(let i=binary.length-1; i>=0; i--,a++) {
            if(binary.charAt(i) === '1') {
                farr[a] = 1;
            } else
                farr[a] = 0;
            pos--;
        }
       for(i =0; i<pos; i++,a++)
            farr[a]=0;
        return farr;
    }  
    
function binStringToSAMFlags(binary) {
        let farr = binStringToSAMFlagsArray(binary);
        let ret = "";
        if(farr[0] === 1) ret +="read paired, ";
        if(farr[1] === 1) ret +="read mapped in proper pair, ";
        if(farr[2] === 1) ret +="read unmapped,";
        if(farr[3] === 1) ret +="mate unmapped, ";
        if(farr[4] === 1) ret +="read reverse strand, ";
        if(farr[5] === 1) ret +="mate reverse strand, ";
        if(farr[6] === 1) ret +="first in pair, ";
        if(farr[7] === 1) ret +="second in pair, ";
        if(farr[8] === 1) ret +="not primary alignment, ";
        if(farr[9] === 1) ret +="read fails platform/vendor quality checks, ";
        if(farr[10] === 1) ret +="read is PCR or optical duplicate, ";
        if(farr[11] === 1) ret +="supplementary alignment";
        return ret;
    }
    
  
module.exports.d2b = dec2bin;
module.exports.flagString = binStringToSAMFlags;
module.exports.flagArr = binStringToSAMFlagsArray;