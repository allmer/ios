const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('./SAMConversions.js');

/** Assumptions
 * input must be in SAM format
 * Test only done for Bowtie2 output
**/

class SAMCounter extends Transform {
    constructor(options,sampleMax=-1,bar=null) {
        super(options);
        this._chunk = "";
        this.sampleMax = sampleMax;
        this.flagCount = [];
        for(let i=0; i<4096; i++)
            this.flagCount[i] = 0;
        this.bar = bar;
    }
    
    _transform(chunk,encoding,done) {
        if(this.bar !== null) this.bar.tick(chunk.length+1); //plus 1 for the \n removed by split2 in the preceding pipeline.
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003') {
            this.push('\u0003');
            process.exit(0);
        }
        if(strChunk.startsWith("@")) {
            done();
        } else {
            if(this.sampleMax === 0) {
                throw("NORMAL_TERMINATION_SET_LIMIT_REACHED");
                //this.destroy();
                //process.exit(0);
            } else {
                if(this.sampleMax > 0)
                    this.sampleMax--;
                let dta = strChunk.split("\t");
                let flag = dta[1].trim();
                this.flagCount[flag]++;
                done();
            }
        }
    }
    getFlagCount(flag) {
        return this.flagCount[flag];
    }
    getFlagCounts() {
        return this.flagCount;
    }
}

module.exports=SAMCounter;