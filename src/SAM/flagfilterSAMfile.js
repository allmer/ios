const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('./SAMConversions.js');
const SAMFlagFilter = require("./SAMFlagFilter");

/** Assumptions
 * input must be in SAM format
 * input must be name sorted
 * Test only done for Bowtie2 output

**/

function usage(err) {
    console.log("Error: ", err);
    console.log("-------------------------------");
    console.log("Usage instructions");
    console.log("-------------------------------");
    console.log("Example: flagfiltersamfile -i example.sam -left 1.fq -f 97,145 -right r.fq");
    console.log("-i %FILE%\tinput file in SAM format");
    console.log("-l %FILE%\tWhere to store /1 reads");
    console.log("-r %FILE%\tWhere to store /2 reads");
    console.log("-m %FILE%\tWhere to store /1 and /2 reads in an interleaved file");
    console.log("-s %FILE%\tWhere to store singlet reads");
    console.log("-f %num,num,...%\tThe flags that should be extracted as reads. If reads are paired flags must also be complementary. -f can be used multiple times and the flags are aggregated");
    console.log("--------------------------------");
    process.exit();
}

let inStream = null, left = null, right = null, singlet = null, interleaved = null;
let inFile = null, leftFile = null, rightFile = null, singletFile = null, interleavedFile = null;
//let mode = true; //Extractonly pairs where neither of the mates is ever aligned (given multimaps).
let flags = []; //Extractonly pairs that are never properly aligned as a pair.

var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;
        case '-l':  i++;
                    leftFile = inArgs[i];
                    break;
        case '-r':  i++;
                    rightFile = inArgs[i];
                    break;
        case '-s':  i++;
                    singletFile = inArgs[i];
                    break;
        case '-m':  i++;
                    interleavedFile = inArgs[i];
                    break;
        case '-f':  i++;
                    let tmp = inArgs[i].trim();
                    if(tmp.includes(",")) {
                        let arr = tmp.split(",");
                        for(let i=0; i<arr.length; i++)
                            flags[arr[i]]=0;
                    } else 
                        flags[tmp]=0;
                    break;
         
        default:    console.log("Unknown switch ",inArgs[i]);
                    process.exit();
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        console.log("An error occured with reading the file provided (",inFile,"): ",err);
        process.exit();
    });
}

if(leftFile !== null) {
    left = fs.createWriteStream(leftFile);
    left.on('error', function(err) {
        console.log("An error occured with the left,/1,first file provided (",leftFile,"): ",err);
        process.exit();
    });
}

if(rightFile !== null) {
    right = fs.createWriteStream(rightFile);
    right.on('error', function(err) {
        console.log("An error occured with the right,/2,second file provided (",rightFile,"): ",err);
        process.exit();
    });
}

if(singletFile !== null) {
    singlet = fs.createWriteStream(singletFile);
    singlet.on('error', function(err) {
        console.log("An error occured with the singleton file provided (",singletFile,"): ",err);
        process.exit();
});
}

if(interleavedFile !== null) {
    interleaved = fs.createWriteStream(interleavedFile);
    interleaved.on('error', function(err) {
        console.log("An error occured with the interleaved file provided (",interleavedFile,"): ",err);
        process.exit();
});
}

console.log("Setting up flag filter.");
let sf = new SAMFlagFilter(null,flags);
if(interleaved !== null) sf.listenInterleaved(interleaved);
if(left !== null) sf.listenLeft(left);
if(right !== null) sf.listenRight(right);
if(singlet !== null) sf.listenSinglet(singlet);
console.log("Starting process.");
pipeline(
        inStream,
        split2(),
        sf,
        err => {
            if(err) {
                console.log("Processing failed: ",err);
            } else {
                if(inStream !== null) inStream.destroy();
                if(left !== null) left.destroy();
                if(right !== null) left.destroy();
                if(singlet !== null) left.destroy();
                if(interleaved !== null) left.destroy();
                console.log('Processing succeeded.');
            }
        }
);
