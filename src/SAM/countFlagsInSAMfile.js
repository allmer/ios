const {pipeline} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');
const fs = require('fs');
const conv = require('./SAMConversions.js');
const SAMCounter = require("./SAMCounter.js");

let instream;
let inFile = "../files/SAMSample.sam";

var inArgs = process.argv.slice(2);
for(let i=0; i<inArgs.length; i++) {
    switch(inArgs[i]) {
        case '-i':  i++;
                    inFile = inArgs[i];
                    break;
        default:    console.log("Unknown switch ",inArgs[i]);
                    process.exit();
    }
}

if(inFile !== null) {
    inStream = fs.createReadStream(inFile);
    inStream.on('error', function(err) {
        console.log("An error occured with reading the file provided (",inFile,"): ",err);
        process.exit();
    });
} else {
    console.log("A SAM file needs to be provided using the -i switch.");
    exit(1);
}


let sc = new SAMCounter(null);
console.log("Starting Count.");
pipeline(
        inStream,
        split2(),
        sc,
        err => {
            if(err) {
                console.log("Processing failed: ",err);
            } else {
                if(inStream !== null) inStream.destroy();
                console.log("The following alignment flags were found in the file (",inFile,"):");
                console.log("flag\tcount\tread paired\tread mapped in proper pair\tread unmapped\tmate unmapped\tread reverse strand\tmate reverse strand\tfirst in pair\tsecond in pair\tnot primary alignment\tread fails platform/vendor quality checks\tread is PCR or optical duplicate\tsupplementary alignment");
                let farr = sc.getFlagCounts();
                for(let f=0; f<farr.length; f++) {
                    if(farr[f]>0) { //an flag with count
                        let bin = conv.d2b(f);
                        let flags = conv.flagArr(bin);
                        console.log(f+"\t"+farr[f]+"\t"+flags.join('\t'));
                    }
                }
            }
        }
);
