import MSSpectrum from '../src/helper/MSSpectrum.js';
import {describe, expect} from "@jest/globals";
import Filter from "../src/helper/Filter.js";

const mgfSection = [
  'BEGIN IONS',
  'PEPMASS=491.222686767578',
  'CHARGE=2',
  'TITLE=491.222686767578_1494.17_scan=6268_2014090922Mix2alkylISW10noEclu,seq={ATNYNAGDR},sup={4}',
  'SCANS=0',
  '491.2227 92',
  '128.1677 34.3',
  '143.9659 14.8',
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '162.1551	94.7',
  '163.1792	69.3'
];

describe('Filters', () => {
  describe('cutoff filter',() => {
    test('filterSpectrum', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterByMinIntensity(msSpectrum.spectrum,100);
      expect(filtered.length).toBe(6);
    });
    test('filterAllPeaksFromSpectrum', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterByMinIntensity(msSpectrum.spectrum,10000);
      expect(filtered.length).toBe(0);
    });  
    test('filterNoPeaksFromSpectrum', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterByMinIntensity(msSpectrum.spectrum,10);
      expect(filtered.length).toBe(12);
    }); 
  });
  describe('percentile filter',() => {
    test('filterBot30Percent', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterBotXPercent(msSpectrum.spectrum,0.3);
      expect(filtered.length).toBe(8);
    });
  });  
  describe('window filter',() => {
    test('filterNperW', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterBestNperWindow(msSpectrum.spectrum,1,5);
      expect(filtered.length).toBe(4);
    });
    test('filterNperW', () => {
      const msSpectrum = new MSSpectrum();
      msSpectrum.parseMGFSection(mgfSection);
      const filtered = Filter.filterBestNperWindow(msSpectrum.spectrum,2,4);
      console.log(filtered,filtered.length);
      expect(filtered.length).toBe(7);
    });    
  });    
});
