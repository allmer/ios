import * as metaData from '../src/TestDataCreation/MetaData.js';
import {describe, expect} from "@jest/globals";


describe('MetaDataTest', () => {
  describe('calcMonoMass', () => {
    test('should succeed (tests monoMass)', () => {
      expect(metaData.synthPeps[0].monoMass).toBeCloseTo(806.67); //Why does MSProduct deviate here (806.4771)?
    });
  });
});
