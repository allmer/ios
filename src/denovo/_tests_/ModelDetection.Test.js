import GraphSpectrum from '../src/helper/GraphSpectrum.js';
import MSSpectrum from '../src/helper/MSSpectrum.js';
import {describe, expect} from "@jest/globals";

const mgfSection = [
  'BEGIN IONS',
  'PEPMASS=1000',
  'CHARGE=1',
  'TITLE=syntheticModelTestSpectrum',
  'SCANS=0',
  '1000\u00092',
  '100  20',
  '143.9659	14.8',
  '145.1864	1063.5',
  '147.2310	164.8',
  '148.0274	88.9',
  '152.2586	32.3',
  '153.1165	141.1',
  '155.0703	453.6',
  '156.2521	121.2',
  '158.0017	158.1',
  '162.1551	94.7',
  '163.1792	69.3'
];
const msms = new MSSpectrum();
msms.parseMGFSection(mgfSection);

describe('ModelDetection', () => {
  describe('green models', () => {
    test('model X', () => {
      const gms = new GraphSpectrum(msms.spectrum,0.3,msms.precursorMass);
      expect(gms.nodeList.length).toBe(12);
      /* TODO */
    });
  });
});
