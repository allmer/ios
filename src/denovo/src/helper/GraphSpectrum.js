import MSEdgeFactory from './MSEdgeFactory.js';
import Node from '../graph/Node.js';
import Util from './Util.js';
import Edge from '../graph/Edge.js';

export default class GraphSpectrum {
  constructor(peaks, tolerance, pm) {
    this._pm = pm;
    this._peaks = peaks;
    this._nodeList = [];
    this._edgeMap = new Map();
    this.tolerance = tolerance;
    this.createNodes();
    this.createDistEdges();
    this.createSequenceEdges();
  }

  createNodes() {
    // { data: { id: 'd' }, position: { x: 215, y: 175 } },
    const width = 40;
    const warp = 50;
    const yOffset = 50;
    const leftMargin = 20;
    const topMargin = 50;
    let line = 0;
    let pos = 0;
    for (let i = 0; i < this._peaks.length; i += 1) {
      line = Math.floor(this._peaks[i].mz / width);
      pos = (this._peaks[i].mz - line * width) * warp;
      const id = this._peaks[i].mz;
      const mz = this._peaks[i].mz;
      const intensity = this._peaks[i].intensity;
      this._nodeList.push(new Node(id, mz, intensity, 1));
    }
  }

  getNodes = charge => {
    if (charge === 1) {
      return this._nodeList;
    }
    let start = 0;
    let end = 0;
    if (charge === 2) {
      start = this._pm / 4;
      end = this._pm / 2;
    }
    if (charge === 3) {
      start = this._pm / 5;
      end = this._pm / 3;
    }
    return this._nodeList.filter(node => {
      return node.mz > start && node.mz < end;
    });
  };

  get nodes() {
    return this._nodeList;
  }

  createDistEdges() {
    /* if(!this._nodeMap.length || this._nodeMap.length <= 0) {
		    this.nodes;
		} */
    const mSEdgeFactory = new MSEdgeFactory();
    for (let s = 0; s < 3; s += 1) {
      const nodes = this.getNodes(s + 1);
      const data = mSEdgeFactory._createEdgeArray(s + 1);
      for (let o = 0; o < nodes.length - 1; o += 1) {
        for (let i = o + 1; i < nodes.length; i += 1) {
          const diff = nodes[i].mz - nodes[o].mz;
          for (let k = 0; k < data.length; k += 1) {
            if (diff > data[k].value + 2 * this.tolerance) break;
            if (diff >= data[k].value - this.tolerance && diff <= data[k].value + this.tolerance) {
              let source;
              let target;
              if (nodes[i].id < nodes[o].id) {
                source = nodes[i].id;
                target = nodes[o].id;
              } else {
                source = nodes[o].id;
                target = nodes[i].id;
              }
              this.addEdge(
                new Edge(
                  Util.getKey(source, target),
                  data[k].color,
                  source,
                  target,
                  data[k].name,
                  diff,
                  data[k].value,
                  s + 1
                ),
                data[k].name
              );
            }
          }
        }
      }
    }
  }

  addEdge(edge, name) {
    if (!this._edgeMap.has(name)) {
      this._edgeMap.set(name, new Array(edge));
    } else {
      const arr = this._edgeMap.get(name);
      arr.push(edge);
    }
  }

  createSequenceEdges() {
    // { data: { id: 'ad', source: 'a', target: 'd' } },
    // console.log(this._nodeMap);
    /*
         if(!this._nodeMap.length || this._nodeMap.length <= 0) {
			await this.nodes;
		}		 */
    const edges = [];
    for (let n = 1; n < this._nodeList.length; n += 1) {
      const prev = this._nodeList[n - 1].id;
      const curr = this._nodeList[n].id;
      edges.push(
        new Edge(
          Util.getKey(prev, curr),
          'sequence',
          prev,
          curr,
          'sequence',
          prev - curr,
          'sequence',
          1
        )
      );
    }
    this._edgeMap.set('sequence', edges);
  }

  getEdges(name) {
    // { data: { id: 'ad', source: 'a', target: 'd' } },
    return this._edgeMap.get(name);
  }

  get nodeList() {
    return this._nodeList;
  }

  get edgeMap() {
    return this._edgeMap;
  }
}
