export default class AminoAcidEdgeFactory {
  constructor() {
    this.edgeArray = [
      { monoisotopicMass: 186.080048, oneLetterCode: 'W' },
      { monoisotopicMass: 163.063974, oneLetterCode: 'Y' },
      { monoisotopicMass: 156.101996, oneLetterCode: 'R' },
      { monoisotopicMass: 147.069074, oneLetterCode: 'F' },
      { monoisotopicMass: 137.059422, oneLetterCode: 'H' },
      { monoisotopicMass: 131.041174, oneLetterCode: 'M' },
      { monoisotopicMass: 129.043074, oneLetterCode: 'E' },
      { monoisotopicMass: 128.095848, oneLetterCode: 'K' },
      { monoisotopicMass: 128.059148, oneLetterCode: 'Q' },
      { monoisotopicMass: 115.027274, oneLetterCode: 'D' },
      { monoisotopicMass: 114.043348, oneLetterCode: 'N' },
      { monoisotopicMass: 113.084874, oneLetterCode: 'L' },
      // { monoisotopicMass: 113.084874, oneLetterCode: 'I' },
      { monoisotopicMass: 103.009574, oneLetterCode: 'C' },
      { monoisotopicMass: 101.048174, oneLetterCode: 'T' },
      { monoisotopicMass: 99.069074, oneLetterCode: 'V' },
      { monoisotopicMass: 97.053274, oneLetterCode: 'P' },
      { monoisotopicMass: 87.032374, oneLetterCode: 'S' },
      { monoisotopicMass: 71.037474, oneLetterCode: 'A' },
      { monoisotopicMass: 57.021674, oneLetterCode: 'G' }
    ];
  }

  pairwise(list) {
    if (list.length < 2) {
      return [];
    }
    const first = list[0];
    const rest = list.slice(1);
    const pairs = rest.map(x => {
      return {
        monoisotopicMass: first.monoisotopicMass + x.monoisotopicMass,
        oneLetterCode: `[${first.oneLetterCode}${x.oneLetterCode}]`
      };
    });
    return pairs.concat(this.pairwise(rest));
  }

  _createAminoAcidEdgeArray() {
    return this.edgeArray;
  }

  aACombs() {
    const pairs = [];
    pairs.push(...this.edgeArray);
    pairs.push(...this.pairwise(pairs));
    return pairs;
  }

  aAPairs() {
    return this.pairwise(this.edgeArray);
  }

  createEdge(key) {
    if (this.aminoAcidEdgeMap === undefined) {
      this.aminoAcidEdgeMap = new Map();
      this._createAminoAcidEdgeArray().forEach(edge => {
        this.aminoAcidEdgeMap.set(edge.oneLetterCode, edge);
      });
    }
    if (key === 'I') {
      return { monoisotopicMass: 113.084874, oneLetterCode: 'I' };
    }
    return this.aminoAcidEdgeMap.get(key);
  }

  findAminoAcid(mass, tol) {
    const pairs = this.aACombs();
    return pairs.find(edge => Math.abs(mass - edge.monoisotopicMass) < tol);
  }
}
