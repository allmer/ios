import * as constants from '../helper/Chemistry.js';

//The synthetic peptides created for the study and measured in several machines with various settings.
let SyntheticPeptides  = [
    {"sequence":"KYIPGTK", "avgMass":806.9862},
    {"sequence":"NKGITWK", "avgMass":847.0108},
    {"sequence":"DYVAQFEASALGK", "avgMass":1399.5526},
    {"sequence":"MGDVEKGK", "avgMass":864.0142},
    {"sequence":"EDLIAYLK", "avgMass":965.1412},
    {"sequence":"CAQCHTVEKGGK", "avgMass":1261.4732},
    {"sequence":"HKTGPNLHGLFGR", "avgMass":1434.6538},
    {"sequence":"YIPGTKMIFAGIK", "avgMass":1439.8144},
    {"sequence":"TGQAPGFTYTDANK", "avgMass":1471.5759},
    {"sequence":"MIFAGIKK", "avgMass":908.2012},
    {"sequence":"HLVDEPQNLIK", "avgMass":1306.5136},
    {"sequence":"LVNELTEFAK", "avgMass":1164.3521},
    {"sequence":"CCTKPESER", "avgMass":1053.2082},
    {"sequence":"QTALVELLK", "avgMass":1015.2455},
    {"sequence":"LCVLHEK", "avgMass":842.0542},
    {"sequence":"TPVSEKVTK", "avgMass":989.1637},
    {"sequence":"SEIAHRFK", "avgMass":988.1411},
    {"sequence":"AEFVEVTK", "avgMass":923.06},
    {"sequence":"SLGKVGTR", "avgMass":817.9693},
    {"sequence":"ATEEQLK", "avgMass":818.9074},
    {"sequence":"EVVGSAEAGVDAASVSEEFR", "avgMass":2010.1345},
    {"sequence":"VYLPRMK", "avgMass":907.1729},
    {"sequence":"GLWEKAFK", "avgMass":979.1739},
    {"sequence":"VASMASEKMK", "avgMass":1082.3343},
    {"sequence":"LTEWTSSNVMEER", "avgMass":1582.7397},
    {"sequence":"GGLEPINFQTAADQAR", "avgMass":1688.8486},
    {"sequence":"LTEWTSSNVMEERK", "avgMass":1710.9149},
    {"sequence":"ISQAVHAAHAEINEAGR", "avgMass":1774.9456},
    {"sequence":"ELINSWVESQTNGIIR", "avgMass":1860.0897},
    {"sequence":"LGMDGYR", "avgMass":811.9405},
    {"sequence":"QYVQGCGV", "avgMass":853.9780},
    {"sequence":"CQNRDVR", "avgMass":891.0023},
    {"sequence":"ATNYNAGDR", "avgMass":982.0033},
    {"sequence":"VVRDPQGIR", "avgMass":1040.2177},
    {"sequence":"ALIVLGLVLLSVTVQGK", "avgMass":1724.1942},
    {"sequence":"STDYGIFQINSR", "avgMass":1401.5279},
    {"sequence":"YWCNDGK", "avgMass":885.9795},
    {"sequence":"WESGYNTR", "avgMass":1013.0607},
    {"sequence":"NDIAAKYK", "avgMass":923.063},
    {"sequence":"LFTGHPETLEK", "avgMass":1272.4523},
    {"sequence":"KHGTVVLTALGGILK", "avgMass":1507.8713},
    {"sequence":"YKELGFQG", "avgMass":942.0657},
    {"sequence":"VEADIAGHGQEVLIR", "avgMass":1607.8178},
    {"sequence":"HGTVVLTALGGILK", "avgMass":1379.6962},
    {"sequence":"HPGDFGADAQGAMTK", "avgMass":1503.6432}
];

//Better to sort the array by mass so that a precursor mass can quickly be matched to a mass in the table.
//associative arrays may speed up the process (see below int map)
SyntheticPeptides.sort(function(a, b) {
    return parseFloat(a.avgMass) - parseFloat(b.avgMass);
});

//Calculation of the monoisotopic masses.
//There seems to be a small difference to what MSProduct calculates which is strange.
export const synthPeps = SyntheticPeptides.map(info => {
    let monoMass = 0;
    for(let i=0; i<info.sequence.length; i++) {
        monoMass += constants.aminoAcidsByName[info.sequence[i]].monoIsotopicMass;
    }
    monoMass += constants.Water.monoIsotopicMass;
    monoMass += constants.Hydrogen.monoIsotopicMass;
    info['monoMass'] = Number(monoMass);  
    return info;
});

let AvgMassIndex = new Map();
for(let i=0; i<synthPeps.length; i++) {
    let indexMass = Math.round(synthPeps[i].avgMass);
    if(indexMass === 923) {
        let tmp = synthPeps[i];
        tmp.sequence = "AEFVEVTK-NDIAAKYK"; //These two peptides have a mass so similar we cannot differentiate them.
        AvgMassIndex.set(923,tmp);          //Perhaps they can be separated by peaks in the spectrum.
    } else
        AvgMassIndex.set(indexMass,synthPeps[i]);
}

//for fast access to the sequence and future other information by the integer part of the precursor mass.
export const intPepMassIndex = AvgMassIndex;