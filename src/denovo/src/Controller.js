import Model from "./Model.js";

export default class Controller {

  static readInput(parameters){
    return Model.readInput(parameters)
  }
  static normalization(parameters){
    parameters.normalization.forEach(norm =>{
      switch (norm.method) {
        case 'normalizeIntensity':
          return Model.normalizeIntensity(parameters)
      }
    });
    return parameters;
  }
  static spectrum_filtering(parameters){
    parameters.spectrum_filtering.forEach(spec_filter =>{
      switch (spec_filter.method) {
        case 'filterSpectrum':
          parameters.cutoff = spec_filter.params[0];
          return Model.filterSpectrum(parameters);
      }
    });
    return parameters;
  }

  static createIonGraph(parameters){
    return Model.createIonGraph(parameters);
  }
  static filterIonGraphNodes(parameters){
    return Model.filterIonGraphNodes(parameters);
  }
  static filterIonGraphEdges(parameters){
    return Model.filterIonGraphEdges(parameters);
  }
  static createModelGraph(parameters){
    return Model.createModelGraph(parameters);
  }
  static filterModels(parameters){
    return Model.filterModels(parameters)
  }
  static createRawBIonGraph(parameters){
    return Model.createRawBIonGraph(parameters)
  }
  static mergeBIons(parameters){
    return Model.mergeBIons(parameters);
  }
  static filterBIons(parameters){
    return Model.filterBIons(parameters);
  }
  static finishBIonGraph(parameters){
    return Model.finishBIonGraph(parameters)
  }
  static filterEdges(parameters) {
    return Model.filterEdges(parameters);
  }
  static findPaths(parameters){
    return Model.findPaths(parameters);
  }
  static scorePaths(parameters) {
    return Model.scorePaths(parameters);
  }
  static rankPaths(parameters) {
    return Model.rankPaths(parameters);
  }
  static correctScorer(parameters){
    return Model.correctScorer(parameters);
  }
  static editDistScorer(parameters){
    return Model.editDistScorer(parameters);
  }
  static writeOutput(parameters){
    return Model.writeOutput(parameters);
  }


}