const {pipeline, Transform} = require('stream');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

/**
 * Blast -outfmt 6 qseqid qlen sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore is the only supported input for this.
 * Filtering is by % of query matched   
**/

class BlastScoreFilter extends Transform {
    constructor(options,minQueryPercMatch=0.8) {
        super(options);
        this.minqpm = minQueryPercMatch;
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let row = strChunk.split("\t");
        let qpm = row[3]/row[1];
        console.log(row);
        console.log(this.minqpm,qpm);
        if(qpm >= this.minqpm) {
            this.push(strChunk+"\n");
            console.log("pushing");
        }
        done();
    }
}


module.exports=BlastScoreFilter;
/* http://www.metagenomics.wiki/tools/blast/blastn-output-format-6
 0.	 qseqid	 query (e.g., gene) sequence id
 1.      qlen    length of the query
 2.	 sseqid	 subject (e.g., reference genome) sequence id
 3.	 pident	 percentage of identical matches
 4.	 length	 alignment length
 5.	 mismatch	 number of mismatches
 6.	 gapopen	 number of gap openings
 7.	 qstart	 start of alignment in query
 8.	 qend	 end of alignment in query
 9.	 sstart	 start of alignment in subject
 10.	 send	 end of alignment in subject
 11.	 evalue	 expect value
 12.	 bitscore	 bit score
*/