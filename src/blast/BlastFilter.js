const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

var qLen = 20;


process.argv.forEach(function (val, index, array) {
  //console.log(index + ': ' + val);
});

class FilterBlastOutput extends Transform {
    constructor(options) {
        super(options);
    }
    
    _transform(chunk,encoding,done) {
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let row = strChunk.split("\t");
        //console.log(row);
        if((row[3] >= 17) || (row[4] >= 4)) {//aln length || mismatches
			//console.log("good");
			this.push(chunk);
			this.push("\n");
			done();					
		} else
			done(); //bad
    }
}


pipeline(
        process.stdin,
        split2(),
        new FilterBlastOutput(),
        process.stdout,
        err => {
            if(err) {
                console.log("Pipeline failed: ",err);
            } else {
                console.log('Pipeline succeeded.');
            }
        }
);



/*
 1.	 qseqid	 query (e.g., gene) sequence id
 2.	 sseqid	 subject (e.g., reference genome) sequence id
 3.	 pident	 percentage of identical matches
 4.	 length	 alignment length
 5.	 mismatch	 number of mismatches
 6.	 gapopen	 number of gap openings
 7.	 qstart	 start of alignment in query
 8.	 qend	 end of alignment in query
 9.	 sstart	 start of alignment in subject
 10.	 send	 end of alignment in subject
 11.	 evalue	 expect value
 12.	 bitscore	 bit score
*/