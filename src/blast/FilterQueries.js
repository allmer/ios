const {pipeline, Transform} = require('stream');
const split2 = require('split2');
const fs = require('fs');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

let inQueries = '';
let outPath = '';
let inPath = '';
function processArguments() {
    inQueries = process.argv[2];
    inPath = process.argv[3];
    outPath = process.argv[4];
}
processArguments();



class FilterQueries extends Transform {
    constructor(options,inPath) {
        super(options);
        var data = fs.readFileSync(inPath,'utf8').split(/\r?\n/);
        var ids = data.filter(line => line.startsWith(">")); 
        var seqs = data.filter(line => !line.startsWith(">")); 
        ids = ids.map(line => line.substring(1));
        this.ids = new Map();
        var idx = 0;
        ids.map(id => {
            this.ids.set(id,seqs[idx++]);
        });
    }
    
    _transform(chunk,encoding,done) {
        //console.log('r');
        let strChunk = decoder.write(chunk);
        if(strChunk === '\u0003')
            process.exit();
        let row = strChunk.split("\t");
        this.ids.delete(row[0]);
        done();
    }
    get remaining() {
        return this.ids;
    }
}

function storeRes(qp) {
    //console.log("storing results");
    var ids = new Set();
    for (const [k, v] of qp.remaining.entries()) {
        outStream.write(">" + k + "\n" + v + "\n");
        ids.add(k.substring(0,k.search('_')));
    }
    ids.forEach(id => outStream.write(id+"\n"));
    return outStream;
   // outStream.end();
}

let inStream = fs.createReadStream(inPath);
let fq = new FilterQueries(null,inQueries);
let outStream = fs.createWriteStream(outPath);

pipeline(
          inStream,
          split2(),
          fq,
          err => {
              if(err) {
                  console.log("Pipeline failed: ",err);
              } else {
                  storeRes(fq);
                  console.log('Queries filtered by BLAST results.');
              }
          }
  );