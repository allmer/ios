const fs = require('fs');
const readline = require("readline");

/** Assumptions
 * A subset of the NCBI Taxonomy is used here with the intent to build an 
 * index so that files (e.g., FASTA) can be filtered at any level quickly.
 * Only names.dmp and nodes.dmp are used at the moment.
**/

class TaxonomyBuilder {
    constructor() {
        this.dirIDtaxonomy = {};
        this.parIDtaxonomy = {};
        this.filled = false;
    }
    
    parseFiles(nameFile="../files/names.dmp",nodeFile="../files/nodes.dmp",bar=null) {
        return new Promise((resolve,reject) => {
            //console.log("Building taxononmy.");
            if(bar !== null) bar.total = fs.statSync(nameFile).size;
            let nameStream = fs.createReadStream(nameFile);
            let nodeStream = fs.createReadStream(nodeFile);
            let naDone = false, noDone = false;
            let nasl = readline.createInterface({
                input: nameStream,
                output: process.stdout,
                terminal: false
            });
            nasl.on('line',(line) => {
                if(bar !== null) bar.tick(line.length+1);
                if(line.includes("scientific name")) {
                    let arr = line.split("\t|\t");
                    if(arr[0] in this.dirIDtaxonomy) {
                         this.dirIDtaxonomy[arr[0]]['taxID']=arr[0];
                         this.dirIDtaxonomy[arr[0]]['name']=arr[1];
                    } else {
                        this.dirIDtaxonomy[arr[0]]={'taxID':arr[0],'name':arr[1]};
                    }
                    //console.log(this.dirIDtaxonomy[arr[0]]);
                }
            });
            let nosl = readline.createInterface({
                input: nodeStream,
                output: process.stdout,
                terminal: false
            });
            nosl.on('line',(line) => {
                let arr = line.split("\t|\t");
                //if(arr[0] == 10088 || arr[1] == 10088) console.log(arr.join());
                //if(arr[0] == 862507 || arr[1] == 862507) console.log(arr.join());
                if(arr[1] in this.parIDtaxonomy) {
                    this.parIDtaxonomy[arr[1]]['children'][arr[0]]=arr[2];
                    //console.log(this.parIDtaxonomy[arr[1]]);
                } else {
                    this.parIDtaxonomy[arr[1]] = {'children':{}};
                    this.parIDtaxonomy[arr[1]]['children'][arr[0]]=arr[2];
                }
                if(arr[0] in this.dirIDtaxonomy) {
                    this.dirIDtaxonomy[arr[0]]["parentID"] = arr[1];
                    this.dirIDtaxonomy[arr[0]]["rank"] = arr[2];
                } else {
                    this.dirIDtaxonomy[arr[0]]={'parentID':arr[1],'rank':arr[2]};
                }
                //console.log(this.dirIDtaxonomy[arr[0]]);
            });
            nosl.on('close', () => {
                noDone = true;
                if(naDone) {
                    //console.log("Done parsing Taxonomy.");
                    this.filled = true;
                    resolve(this);
                }
            });
            nasl.on('close', () => {
                naDone = true;
                if(noDone) {
                    //console.log("Done parsing Taxonomy.");
                    this.filled = true;
                    resolve(this);
                }
            });
        });
    }
    
    getTaxonomy(which='dir') {
        switch(which) {
            case 'dir': return this.dirIDtaxonomy;
            case 'par': return this.parIDtaxonomy;
            default: throw Error("No such taxonomy sorting: "+which+" (TaxonomyBuilder.getTaxonomy(which)).");
        }
    }
    
    getTaxIDsUnder(taxID) {
        if(!this.filled) 
            throw Error('The taxonomy needs to be build first. Use parseFiles to do so.');
        else {
            return new Promise((resolve,reject) => {
                console.log("Extracting tax ids under "+taxID+".");
                let ret = {};
                let done = false;
                let parents = [];
                parents[0] = taxID;
                let o=0;
                while(!done) {
                    let np = [];
                    let p = 0;
                    for(let i=0; i<parents.length; i++) {
                        if(!(parents[i] in this.dirIDtaxonomy)) {
                            console.log("Problem with ", parents[i]);
                            continue;
                        }
                        ret[parents[i]] = this.dirIDtaxonomy[parents[i]].name;
                        ret[this.dirIDtaxonomy[parents[i]].name] = parents[i];  
                        if(!(parents[i] in this.parIDtaxonomy)) { //found lowest level which may or may not be a species.
                            continue;
                        }                        
                        let nc = Object.keys(this.parIDtaxonomy[parents[i]].children).length;
                        if(nc === 0) { //found lowest level which may or may not be a species.
                            ret[parents[i]] = this.dirIDtaxonomy[parents[i]].name;
                            ret[this.dirIDtaxonomy[parents[i]].name] = parents[i];
                        } else {
                            for(var child in this.parIDtaxonomy[parents[i]].children) {
                                if(this.dirIDtaxonomy[child].rank !== "species" && this.dirIDtaxonomy[child].rank !== "no rank") {
                                    np[p++] = child;
                                } else {
                                    ret[child] = this.dirIDtaxonomy[child].name;
                                    ret[this.dirIDtaxonomy[child].name] = child;
                                }
                            }
                        }
                    }
                    if(np.length === 0)
                        done = true;
                    else 
                        parents = np;
                }
                resolve(ret);
            });
        }
    }
}

module.exports=TaxonomyBuilder;