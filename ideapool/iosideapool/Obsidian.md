# Obsidian
## Plugins
Obsidian has a lot of plugins, interfacing with typical management tools or providing kanban views. 

## Features
The search functionality is quite useful as it allows to find keywords spread over different md pages and provides a bit of context for each match. 

## Missing
### Links
I couldn't find a quick way to establish [links](Connectivity) between md files. This needs to be done via adding hyper links. Ultimatively, there are two major problems with this:
1. linking is to coarse grained (on the md file level)
2. Links need to be established manually (They should be establishable both manually and automatically).
3. Links seem to be undirected and unspecified (there should be many different types of links such as created, edited, supports, any verb in general).
4. Links are not multiple or changing in strenght even if several links go from one to another md file.

### Metadata
There should be meta data included in the ideas or [nodes](Connectivity). For example when a document was first created and last changed. Perhaps other information, although this could alternatively be stored in suitable edges such as created linked to a data, a creator, etc.

### Updates and Notifications
[I](JA) could not find which files were added or modified using Obsidian, but maybe someone else can tell us how to. This is an essential feature in [my](JA) opinion.