# People
## Questions
- What would be a good term to refer to participants in the knowledge base?
- Should there be different people types? E.g., laymen, students, researchers, etc.?
- Should there be a hierarchy established?
- Should a third party vouch for people (researcher id, social media, etc.)?

# Node Type
People cleary constitute a [node type](Connectivity.md) in the knowledge graph, but perhaps AIs or other Agents may be grouped under this or derived node types.


