# Visualization
Ideas are connected and this connection must be visible. That does not imply that an idea is necessarily connected to another idea via data or any other tangible form. For example, the image of a jaguar may remind the car brand and vice versa. But even more remote links should be representable through direct drawing of connections among ideas.

## Nodes

## Edges
Edges in the knowledgegraph connect data, ideas, concepts, etc. Clearly, one edge needs to point to the person first postulating something and everyone who edits, adds, etc.
