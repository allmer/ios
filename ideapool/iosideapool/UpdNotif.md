# Updates
Updates to the idea pool should be visible. 
At least for the topics which the [user](People) is connected to by some [edge](Connectivity) (e.g., by stated interest).

# Notifications
[Users](People) should be notified if something matching their interest or anything they posted is edited, deleted, etc.

